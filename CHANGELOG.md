# Sturdy Platform CHANGELOG

This file is used to list major changes to the repo.

## Unreleased

## 1.21.4 (2019-06-05)
* rds: don't specify version when creating from snapshot

## 1.21.3 (2019-05-24)
* hooks: Bugfix support for stacker-1.6.0 with UnboundVariable

## 1.21.2 (2019-05-20)
* lambda_ebs_snapshot: Better error handling

## 1.21.1 (2019-03-29)
* Fix output to match v1.21 update

## 1.21.0 (2019-03-29)
* Added ability to customize username (DBUsername) for RDS Instance MasterUsername.

## 1.20.0 (2019-02-27)
* Python 3 compatibility fixes

## 1.19.5 (2019-02-26)
* Stacker 1.6 lookup fixes for core hooks
* Python 3 compatibility fixes
* Migrate to new awacs PolicyDocument class (https://github.com/cloudtools/awacs/pull/81)

## 1.19.4 (2018-08-06)
* Python 3 compatibility fixes

## 1.19.3 (2018-05-15)
* lambda_ebs_snapshot: fix incorrectly required parameters

## 1.19.2 (2018-05-15)
* core: fix cw_alarm incorrectly required parameters

## 1.19.1 (2018-04-26)
* core: fix cookbook archive hook for stacker > 1.2

## 1.19.0 (2018-02-12)
* core: add `s3.empty_bucket` hook
* iam: add CFN GetTemplate permissions to `platform_policy`

## 1.18.1 (2018-02-05)
* monitoring: add option to disable ssl verification

## 1.18.0 (2018-02-05)
* monitoring: fix `lambda_http_check` dimension, example, and README

## 1.17.0 (2018-01-25)
* iam: add `serverless` IamPermissions option to `platform_policy`

## 1.16.1 (2018-01-25)
* iam: add CFN Serverless permissions to `platform_policy`

## 1.16.0 (2018-01-24)
* core: add managed policy to `tf_state`

## 1.15.1 (2018-01-23)
* iam: add platform artifact download permissions to `platform_policy`

## 1.15.0 (2018-01-23)
* add `iam` stacker module

## 1.14.0 (2018-01-22)
* rds: update mssql and postgres versions

## 1.13.0 (2018-01-22)
* core: add `keypair` hook to ensure VPN server SSH key exists (if specified)

## 1.12.1 (2018-01-05)
* jenkins: add getparameter iam permission to match existing getparameters
    * getparameters is sufficient for the jenkins codebuild plugin, but other tooling (i.e. awscli) may use only getparameter

## 1.12.0 (2018-01-04)
* jenkins: add rds permissions option

## 1.11.0 (2018-01-03)
* asg: add MetricsCollection parameter to standalone ASG

## 1.10.1 (2018-01-03)

* jenkins: add DescribeStackEvents to Jenkins IAM permissions
    * Should fix Stacker tailing CFN events

## 1.10.0 (2017-12-28)

* core: add platform s3 GetObject permissions to common managed policy

## 1.9.1 (2017-12-13)

* core: drop `allowed_values` from VPN server

## 1.9.0 (2017-12-12)

* update SSM parameter permissions to support SSM parameter hierarchies

## 1.8.6 (2017-12-07)

* lambda_ebs_snapshot: add zip file deployment by default (fix 4096 character limit)

## 1.8.5 (2017-11-27)

* lambda_ebs_snapshot: fix typo in backup script; update to use logging module

## 1.8.4 (2017-11-27)

* jenkins: fix blueprint module imports

## 1.8.3 (2017-11-27)

* core: add windows 2016 to AMI lookup function

## 1.8.2 (2017-11-21)

* core: add `random` option to ssm hook to randomly generate a password

## 1.8.1 (2017-11-20)

* asg: Fix Windows typo

## 1.8.0 (2017-11-20)

Upgrade notes:
In the jenkins config, a CFN lookup to `${customer}-common-core-chefbuckets::ChefBucketArn` has been added (similar to the existing `${customer}-common-core-chefbuckets::ChefDataBucketArn` lookup). If does not match the deployed stack name, override the variable as appropriate.

Changes:
* jenkins: add `jenkins_iam_permissions` environment variable support
* asg: Cleanup cfn-signal calls

## 1.7.0 (2017-11-17)

* asg: add `replacing_immutable` template

## 1.6.2 (2017-11-17)

* core: add `FunctionRegion` to ami_lookup outputs

## 1.6.1 (2017-11-16)

* rds: add `RdsServerSecurityGroup` output

## 1.6.0 (2017-11-15)

* rds: add support for additional tags & overriding `DBInstanceIdentifier`

## 1.5.0 (2017-11-10)

* asg: add support for windows userdata

## 1.4.40 (2017-11-08)

* jenkins: add optional iam managed policies parameter for CodeBuild role

## 1.4.39 (2017-11-07)

* core: update common iam permissions to allow GetBucketVersioning on common buckets
    * This is required for the Jenkins CodeBuild plugin and is safe to grant across systems

## 1.4.38 (2017-11-06)

* jenkins: fix incorrect assume role policy

## 1.4.37 (2017-11-03)

* jenkins: add optional codebuild role & statements

## 1.4.36 (2017-10-20)

* asg: fix CFN parameter description
* core: fix sample environment for stacker v1.1+ compatibility
    * Previous use of single quotes does not appear to work properly with remote_configs

## 1.4.35 (2017-10-11)

* asg: Change OtherTagNames local parameters to OtherTags, and add support for having a different CFN parameter name than the tag name.
    * See the updated AD & jenkins configs for usage examples

## 1.4.34 (2017-10-10)

* asg: Update userdata to allow regular user access to chef-client cache directory
    * This fixes the use of resources like `jenkins_plugin` which has the jenkins user access cached files

## 1.4.33 (2017-10-06)

* asg: Add support for additional ebs volume tags
* asg: Add support for additional hardcoded autoscaling tags
* jenkins: Add module
* activedirectory: Add automatic volume backup tags

## 1.4.32 (2017-10-04)

* asg: Add support for additional autoscaling tags

## 1.4.31 (2017-10-02)

* activedirectory: Add sysvol replication. README cleanup

## 1.4.30 (2017-09-27)

* core: Add ssm:DescribeParameters * permissions to base policy
    * Will allow wildcard lookups of parameters under `all.`

## 1.4.29 (2017-09-27)

* core: add value & value_output options to ssm hook to allow automatic parameter population
* activedirectory: add AD DC ip to parameter store automatically

## 1.4.28 (2017-09-27)

* activedirectory: switch back to Ubuntu 17.04 by default

## 1.4.27 (2017-09-26)

* asg: install aws-cli by default on amazon linux systems
* activedirectory: consolidation and cleanup

## 1.4.26 (2017-09-22)

* rds: Change default for Snapshot Id to allow 'undefined' value
* activedirectory: add VPC CIDR to Chef attributes

## 1.4.25 (2017-09-20)

* activedirectory: Preliminary external trust access support

## 1.4.24 (2017-09-20)

* rds: removed unneeded allowed_values (instance type) & default VPC

## 1.4.23 (2017-09-20)

* asg: add parameter for association of public IP addresses

## 1.4.22 (2017-09-20)

* activedirectory: fix security group update in v1.4.20 (now can be overridden in consuming repos)

## 1.4.21 (2017-09-20)

* asg: Add EIP blueprint
* activedirectory: split ad1 config in to separate configs for 'public' vs 'private' deployments (static public address vs static private address)

## 1.4.20 (2017-09-20)

* core: Add CidrBlock output to VPC
* activedirectory: Update security group to restrict traffic to VPC by default

## 1.4.19 (2017-09-19)

* activedirectory: env rename: `activedirectory_chef_run_list` -> `activedirectory_chef_run_list_1`

## 1.4.18 (2017-09-18)

* activedirectory: rename first domain controller config to ad1

## 1.4.17 (2017-09-12)

* activedirectory: change standalone domainjoinuser password to "all" SSM environment

## 1.4.16 (2017-09-12)

* activedirectory: add domainjoinuser password storage in SSM

## 1.4.15 (2017-09-08)

* activedirectory: add missing samba RPC security group ports

## 1.4.14 (2017-09-08)

* activedirectory: add ICMP to security group ports

## 1.4.13 (2017-09-08)

* activedirectory: add additional security group ports to match Microsoft guidelines

## 1.4.12 (2017-09-01)

* activedirectory: add LDAP to security group ports

## 1.4.11 (2017-08-30)

* asg/activedirectory: add alb support

## 1.4.10 (2017-08-29)

* revert v1.4.10 changes - no longer necessary

## 1.4.9 (2017-08-27)

* activedirectory: add ldaps proxy port 10636 to security group

## 1.4.8 (2017-08-23)

* activedirectory: update single-server config to add domain route53 entry

## 1.4.7 (2017-08-21)

* core: add s3_cache hook
* activedirectory: use s3_cache hook

## 1.4.6 (2017-08-19)

* rds: bump mysql version to v5.7.17

## 1.4.5 (2017-08-18)

* core: add Terraform state blueprint & config file

## 1.4.4 (2017-08-18)

* activedirectory: add route53 record name to chef environment variables

## 1.4.3 (2017-08-16)

* add activedirectory module

## 1.4.2 (2017-08-16)

* asg: fix standalone app server SSM GetParameters permissions to match S3 scheme (ENV/BUCKETKEY/X)

## 1.4.1 (2017-08-16)

* core: add ssm hook

## 1.4.0 (2017-08-15)

* asg: update chef-client default to v13
* asg: add eni blueprint

## 1.3.4 (2017-08-13)

* core: fix subnet lookup regression introduced in v1.3.2

## 1.3.3 (2017-08-12)

* core: add (optional) additional_dirs_relative_paths to cookbook archive hook

## 1.3.2 (2017-08-11)

* core: Add subnet availability zones to outputs
* asg: add ebs_vol blueprint
* asg: update standalone blueprint to grant volume mounting, s3 access, and ssm access

## 1.3.1 (2017-08-11)

* core: Change `AlertTopicArn` CloudWatch alarm parameter to comma delimited list
    * Backwards compatible with existing single topic uses, but allows multiple ARNs to be specified
* core: Make `CustomerName` CloudWatch alarm parameter required
    * Should in practice be backwards compatible with all existing deployments of the template
* rds: Fix optional SNSTopic parameter
    * Now properly set to a comma delimited list to match the type listed in CFN documentation and to allow multiple topics to be specified

## 1.3.0 (2017-08-10)

* Add initial version of asg module
* Add `ComparisonOperator` parameter to core cw_alarm blueprint

## 1.2.10 (2017-08-10)

* core: fix ubuntu ami lookup size error introduced in v1.2.10
    * Inline code is now beyond 4k character limit and must be uploaded from S3

## 1.2.9 (2017-08-10)

* core: fix ubuntu ami lookup error introduced in v1.2.7; additional ami lookup function cleanup

## 1.2.8 (2017-08-10)

* Drop version number from remaining lambda functions that used it
    * As noted in 1.2.6, the generated changesets cause far too much noise
* core: update cookbook_archive hook to support xref lookups

## 1.2.7 (2017-08-09)

* core: update ami lookup to support all ubuntu versions

## 1.2.6 (2017-08-07)

* add monitoring module
* core: drop version number from subnet lookup function
    * Updates to this function cause a large (harmless) changeset

## 1.2.5 (2017-08-07)

* core: add SNS subscription blueprint

## 1.2.4 (2017-08-07)

* core: update vpn_server to accommodate legacy deployments

## 1.2.3 (2017-08-07)

* Add yaml linting; slight yaml cleanup to fix linting errors

## 1.2.2 (2017-08-07)

* Update core with revised Stacker post-1.0.4 hook syntax

## 1.2.1 (2017-08-04)

* Centralize version numbers

## 1.2.0 (2017-08-03)

* Add Stacker rds

## 1.1.0 (2017-08-03)

* Add Stacker lambda_ebs_snapshot

## 1.0.1 (2017-08-02)

* Rename stacker modules directory to prevent conflicts with `stacker.X` python imports

## 1.0.0 (2017-08-02)

* Add core network
    * Separated AMI lookup function into independent stack
    * Remove chef-client run document parameters (to allow permission to execute the document to be granted more broadly)
