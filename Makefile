# Invoke Makefile.py

test:
	./Makefile.py test

lint:
	./Makefile.py lint

unittest:
	./Makefile.py unittest

tar:
	./Makefile.py tar

upload:
	./Makefile.py upload
