#!/usr/bin/env python
"""Lambda HTTP Checker."""
from __future__ import print_function
from os import path
from os.path import dirname, realpath
import sys

from troposphere import (
    AWSHelperFn, If, Equals, Export, GetAtt, Join, Output, Ref, Sub, awslambda,
    iam
)

import awacs.s3
import awacs.cloudwatch
import awacs.awslambda
import awacs.logs
import awacs.sns
import awacs.sts
from awacs.aws import Allow, Statement, PolicyDocument, Principal

from stacker.blueprints.base import Blueprint
from stacker.blueprints.variables.types import CFNCommaDelimitedList, CFNString

from stacker.lookups.handlers.file import parameterized_codec

AWS_LAMBDA_DIR = path.join(
    dirname(dirname(dirname(dirname(realpath(__file__))))),
    'src',
    'lambda_functions'
)
IAM_ARN_PREFIX = 'arn:aws:iam::aws:policy/service-role/'


class LambdaHttpCheck(Blueprint):
    """Extends Stacker Blueprint class."""

    function_src = parameterized_codec(
        open(path.join(AWS_LAMBDA_DIR, 'http_checker.py'), 'r').read(),
        False  # disable base64 encoding
    )

    VARIABLES = {
        'FunctionSrc': {'type': AWSHelperFn,
                        'description': 'Lambda function code',
                        'default': function_src},
        'FunctionName': {'type': CFNString,
                         'description': 'Lambda function supplementary name',
                         'default': 'httpchecker'},
        'CustomerName': {'type': CFNString,
                         'description': 'The Customer Name'},
        'EnvironmentName': {'type': CFNString,
                            'description': 'The Environment Name'},
        'FunctionMemorySize': {'type': CFNString,
                               'description': 'How much memory do you want to '
                                              'allocate to this function?',
                               'default': '512'},
        'FunctionHandler': {'type': CFNString,
                            'description': 'What is the handler name for the '
                                           'function',
                            'default': 'index.lambda_handler'},
        'FunctionRuntime': {'type': CFNString,
                            'description': 'Which runtime should be used for '
                                           'the function?',
                            'allowed_values': ['nodejs4.3', 'python2.7'],
                            'default': 'python2.7'},
        'FunctionTimeout': {'type': CFNString,
                            'description': 'How long should the function be '
                                           'allowed to run',
                            'default': '30'},
        'CreateInVPC': {'type': CFNString,
                        'description': 'Do you want to create a lambda inside '
                                       'the vpc?',
                        'allowed_values': ['true', 'false'],
                        'default': 'false'},
        'CreateExternalLambda': {'type': CFNString,
                                 'description': 'Do you want to create a '
                                                'lambda outside the vpc?',
                                 'allowed_values': ['true', 'false'],
                                 'default': 'true'},
        'FunctionSecurityGroups': {'type': CFNCommaDelimitedList,
                                   'description': 'Security groups to apply '
                                                  'to the lambda.',
                                   'default': ''},
        'FunctionSubnets': {'type': CFNCommaDelimitedList,
                            'description': 'Subnets in which to deploy the '
                                           'internal lambda function.',
                            'default': ''}
    }

    def add_conditions(self):
        """Add conditions to template."""
        template = self.template
        variables = self.get_variables()

        template.add_condition(
            'CreateLambdaInVPC',
            Equals(variables['CreateInVPC'].ref, 'true')
        )
        template.add_condition(
            'CreateExternalLambda',
            Equals(variables['CreateExternalLambda'].ref, 'true')
        )

    def add_resources_and_outputs(self):
        """Add resources and outputs to template."""
        template = self.template
        variables = self.get_variables()

        lambda_iam_role = template.add_resource(
            iam.Role(
                'LambdaRole',
                ManagedPolicyArns=If(
                    'CreateLambdaInVPC',
                    [IAM_ARN_PREFIX + 'AWSLambdaVPCAccessExecutionRole'],
                    [IAM_ARN_PREFIX + 'AWSLambdaBasicExecutionRole']
                ),
                AssumeRolePolicyDocument=PolicyDocument(
                    Version='2012-10-17',
                    Statement=[
                        Statement(
                            Effect=Allow,
                            Action=[awacs.sts.AssumeRole],
                            Principal=Principal('Service',
                                                ['lambda.amazonaws.com'])
                        )
                    ]
                ),
                Path='/',
                Policies=[
                    iam.Policy(
                        PolicyName=Join('-',
                                        [variables['CustomerName'].ref,
                                         'lambda',
                                         variables['FunctionName'].ref,
                                         variables['EnvironmentName'].ref]),
                        PolicyDocument=PolicyDocument(
                            Version='2012-10-17',
                            Statement=[
                                Statement(
                                    Action=[
                                        awacs.cloudwatch.PutMetricData,
                                        awacs.cloudwatch.ListMetrics
                                    ],
                                    Effect=Allow,
                                    Resource=['*'],
                                    Sid='CloudWatchMetrics'
                                )
                            ]
                        ),
                    )
                ]
            )
        )

        lambda_function = template.add_resource(
            awslambda.Function(
                'LambdaFunction',
                Condition='CreateLambdaInVPC',
                Description='Internal HTTP Checker',
                Code=awslambda.Code(
                    ZipFile=variables['FunctionSrc']
                ),
                Handler=variables['FunctionHandler'].ref,
                Role=GetAtt(lambda_iam_role, 'Arn'),
                Runtime=variables['FunctionRuntime'].ref,
                Timeout=variables['FunctionTimeout'].ref,
                MemorySize=variables['FunctionMemorySize'].ref,
                FunctionName=Join('-',
                                  [variables['CustomerName'].ref,
                                   variables['FunctionName'].ref,
                                   'int',
                                   variables['EnvironmentName'].ref]),
                VpcConfig=awslambda.VPCConfig(
                    SecurityGroupIds=variables['FunctionSecurityGroups'].ref,
                    SubnetIds=variables['FunctionSubnets'].ref
                )
            )
        )

        ext_lambda_function = template.add_resource(
            awslambda.Function(
                'LambdaFunctionExt',
                Condition='CreateExternalLambda',
                Description='External HTTP Checker',
                Code=awslambda.Code(
                    ZipFile=variables['FunctionSrc']
                ),
                Handler=variables['FunctionHandler'].ref,
                Role=GetAtt(lambda_iam_role, 'Arn'),
                Runtime=variables['FunctionRuntime'].ref,
                Timeout=variables['FunctionTimeout'].ref,
                MemorySize=variables['FunctionMemorySize'].ref,
                FunctionName=Join('-',
                                  [variables['CustomerName'].ref,
                                   variables['FunctionName'].ref,
                                   'ext',
                                   variables['EnvironmentName'].ref])
            )
        )

        template.add_output(
            Output(
                'FunctionExtArn',
                Condition='CreateExternalLambda',
                Description='External Lambda Function Arn',
                Export=Export(Sub('${AWS::StackName}-%s' % 'FunctionExtArn')),
                Value=GetAtt(ext_lambda_function, 'Arn')
            )
        )

        template.add_output(
            Output(
                'FunctionExtName',
                Condition='CreateExternalLambda',
                Description='External Lambda Function Name',
                Export=Export(Sub('${AWS::StackName}-%s' % 'FunctionExtName')),
                Value=Ref(ext_lambda_function)
            )
        )

        template.add_output(
            Output(
                'FunctionIntArn',
                Condition='CreateLambdaInVPC',
                Description='Internal Lambda Function Arn',
                Export=Export(Sub('${AWS::StackName}-%s' % 'FunctionIntArn')),
                Value=GetAtt(lambda_function, 'Arn')
            )
        )

        template.add_output(
            Output(
                'FunctionIntName',
                Condition='CreateLambdaInVPC',
                Description='Internal Lambda Function Name',
                Export=Export(Sub('${AWS::StackName}-%s' % 'FunctionIntName')),
                Value=Ref(lambda_function)
            )
        )

    def create_template(self):
        """Create template (main function called by Stacker)."""
        self.template.add_version('2010-09-09')
        self.template.add_description("Sturdy Platform - Monitoring - Lambda "
                                      "HTTP Checker "
                                      "- {0}".format(version()))
        self.add_conditions()
        self.add_resources_and_outputs()


def version():
    """Call version function from top of repo."""
    root_dir = dirname(dirname(dirname(dirname(realpath(__file__)))))
    if root_dir not in sys.path:
        sys.path.append(root_dir)
    import platform_version  # pylint: disable=import-error
    return platform_version.version()


# Helper section to enable easy blueprint -> template generation
# (just run `python <thisfile>` to output the json)
if __name__ == "__main__":
    from stacker.context import Context

    print(LambdaHttpCheck('test',
                          Context({'namespace': 'test'}),
                          None).to_json())
