#!/usr/bin/env python
"""Lambda HTTP Checker Cloudwatch Event."""
from __future__ import print_function
from os.path import dirname, realpath
import sys

from troposphere import (
    AWSHelperFn, GenericHelperFn, GetAtt, Join, awslambda, events
)

from stacker.blueprints.base import Blueprint
from stacker.blueprints.variables.types import CFNString


class CloudWatchEvent(Blueprint):
    """CloudWatch events blueprint."""

    VARIABLES = {
        'FunctionInput': {
            'type': AWSHelperFn,
            'description': 'Input for lambda function',
            'default': GenericHelperFn({
                "checks":
                    [{"id": "metric_name",
                      "url": "http://github.com",
                      "status_code": "200"}]})
        },
        'CustomerName': {
            'type': CFNString,
            'description': 'The customers name'
        },
        'EnvironmentName': {
            'type': CFNString,
            'description': 'Name of Environment'
        },
        'FunctionArn': {
            'type': CFNString,
            'description': 'Lambda Function ARN'
        },
        'RuleName': {
            'type': CFNString,
            'description': 'Name prefix for CloudWatch rule; will have '
                           'environment appended to it. Must be unique per '
                           'environment)'
        },
        'ScheduleExpression': {
            'type': CFNString,
            'description': 'Expression for the rule to be scheduled'
        },
    }

    def add_resources_and_outputs(self):
        """Add resources and outputs to template."""
        template = self.template
        variables = self.get_variables()

        cloudwatchrule = template.add_resource(events.Rule(
            'CloudwatchRule',
            Description='Rule to invoke lambda http checker on scheduled '
                        'basis',
            Name=Join('-', [variables['RuleName'].ref,
                            variables['EnvironmentName'].ref]),
            ScheduleExpression=variables['ScheduleExpression'].ref,
            State='ENABLED',
            Targets=[events.Target(
                'CWETarget',
                Arn=variables['FunctionArn'].ref,
                Input=variables['FunctionInput'],
                Id=Join('-', [variables['RuleName'].ref,
                              'tgt',
                              variables['EnvironmentName'].ref])
            )]
        ))

        template.add_resource(
            awslambda.Permission(
                'CloudWatchEventsLambdaPermission',
                Action='lambda:InvokeFunction',
                FunctionName=variables['FunctionArn'].ref,
                Principal='events.amazonaws.com',
                SourceArn=GetAtt(cloudwatchrule, 'Arn')
            )
        )

    def create_template(self):
        """Create template (main function called by Stacker)."""
        self.template.add_version('2010-09-09')
        self.template.add_description("Sturdy Platform - Monitoring - "
                                      "CloudWatch Event "
                                      "- {0}".format(version()))
        self.add_resources_and_outputs()


def version():
    """Call version function from top of repo."""
    root_dir = dirname(dirname(dirname(dirname(realpath(__file__)))))
    if root_dir not in sys.path:
        sys.path.append(root_dir)
    import platform_version  # pylint: disable=import-error
    return platform_version.version()


# Helper section to enable easy blueprint -> template generation
# (just run `python <thisfile>` to output the json)
if __name__ == "__main__":
    from stacker.context import Context

    print(CloudWatchEvent('test',
                          Context({'namespace': 'test'}),
                          None).to_json())
