# Monitoring

## HTTP Checks

Use one of the following sample configurations as a template for using this blueprint in your repository (updating all TBD values, repo tag, etc)

### Simple External Check

Lambda function (common environment):
```
namespace: ${namespace}
stacker_bucket: ${stacker_bucket_name}

package_sources:
  git:
    - uri: git@bitbucket.org:nbdev/sturdy-platform-infrastructure.git
      tag: vX.X.X
      paths:
        - stacker_platform_modules/monitoring
      configs:
        - stacker_platform_modules/monitoring/external-lambda-http.yaml
```

Invoking the function (see `sample_input.json` for an example of the data structure for FunctionInput)
```
namespace: ${namespace}
stacker_bucket: ${stacker_bucket_name}

package_sources:
  git:
    - uri: git@bitbucket.org:nbdev/sturdy-platform-infrastructure.git
      tag: vX.X.X
      paths:
        - stacker_platform_modules/core
        - stacker_platform_modules/monitoring

stacks:
  - name: application-monitor
    class_path: monitoring_blueprints.cw_event.CloudWatchEvent
    variables:
      RuleName: TBD # e.g. myappcheck
      ScheduleExpression: rate(5 minutes)
      FunctionInput: ${file parameterized:file://TBD.json}
      CustomerName: ${customer}
      EnvironmentName: ${environment}
      FunctionArn: ${xref ${customer}-common-monitoring-external-http::FunctionExtArn}
  - name: application-alarm
    class_path: core_blueprints.cw_alarm.CwAlarm
    requires:
      - application-monitor
    variables:
      AlarmDescription: Alarms when the application HTTP health check fails
      MetricName: HTTPResponseCodeMatch
      DimensionName: Check ID
      DimensionValue: CHECKIDHERE  # From the ID of check
      AlarmThreshold: 0
      AlarmPeriod: 60
      Statistic: Sum
      TreatMissingData: breaching
      Namespace: Sturdy Lambda Checks
      AlertTopicArn: ${xref ${customer}-common-core-alert::TopicARN}
      CustomerName: ${customer}
      EnvironmentName: ${environment}
```
