# Jenkins

This module supports deploying automated Jenkins systems.

## Prerequisites

* Defined stacker environment variables:
```
jenkins_route53_hostedzone: XXXX e.g. Z1KU1234567
```

### Optional Environment Variables

* `jenkins_iam_permissions`:
```
jenkins_iam_permissions: stacker_bucket,packer,asg
```
* List of IAM permissions to add to the jenkins server policy:
    * `stacker_bucket`: Allows management of any S3 bucket starting with `stacker-`
    * `chef_configs`: Allows cookbook archives to be uploaded to Chef config bucket
    * `ssm`: Allows for management of SSM parameters under ENV.jenkins.* (e.g. `common.jenkins.*`)
    * `cloudformation`: Basic CFN management
    * `packer`: Adds [basic permissions for running Packer.](https://www.packer.io/docs/builders/amazon.html#using-an-iam-task-or-instance-role)
    * `asg`: Adds permissions for deploying the `asg` Sturdy Platform module
    * `rds`: Adds permissions for deploying the `rds` Sturdy Platform module

## Single Server

Consume in your environment with a config like the following:
```
namespace: ${namespace}
stacker_bucket: ${stacker_bucket_name}

package_sources:
  git:
    - uri: git@bitbucket.org:nbdev/sturdy-platform-infrastructure.git
      tag: vX.X.X
      paths:
        - stacker_platform_modules/jenkins
        - stacker_platform_modules/asg
        - stacker_platform_modules/core
      configs:
        - stacker_platform_modules/jenkins/jenkins.yaml

# To run Jenkins on a Windows server (delete if not applicable):
# stacks:
#   jenkins-server:
#     variables:
#       AppOS: windows-2016
#       AppVolumeSize: 60

# To switch to new availability zone, create a new jenkins-volume stack and
# update the snapshot ID & AZ values. After deployment, terminate the existing
# jenkins instance to redeploy it in the new AZ, and (optionally) delete the
# old volume stack(s)
#
# stacks:
#   jenkins-volume:
#     enabled: false
#   jenkins-volume-az2-1:
#     class_path: asg_blueprints.ebs_vol.EbsVol
#     variables:
#       CustomerName: ${customer}
#       EnvironmentName: ${environment}
#       ApplicationName: jenkins
#       AvailabilityZone: ${xref ${customer}-common-core-vpc::PriSubnet2AZ}
#       SnapshotId: snap-0123abcd
#       Size: '40' # Change to 0 to use the snapshot's size
#       VolumeType: gp2
#       OtherTags:
#         TagBackup:  # for use by the auto backup lambda function
#           Name: Backup
#           Value: Hourly
#   jenkins-server:
#     variables:
#       AppPolicies: ${xref ${customer}-common-core-roles::CommonPolicy},${output jenkins-volume-az2-1::VolumePolicy},${output jenkins-iam::ServerPolicy}
#       AppSubnets: ${xref ${customer}-common-core-vpc::PriSubnet2}
#       ChefAttributeValues:
#         PersistentVolumeId: ${output jenkins-volume-az2-1::VolumeId}
```
