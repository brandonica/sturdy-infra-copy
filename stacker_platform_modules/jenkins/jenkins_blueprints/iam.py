#!/usr/bin/env python
"""Module with jenkins IAM roles/policies."""
from __future__ import print_function
from collections import OrderedDict
from os.path import dirname, realpath
import sys

from troposphere import And, Equals, GetAtt, If, Join, Not, Output, Ref, iam

import awacs.autoscaling
import awacs.awslambda
import awacs.cloudformation
import awacs.codebuild
import awacs.ec2
import awacs.iam
import awacs.logs
import awacs.route53
import awacs.s3
import awacs.ssm
import awacs.sts
from awacs.aws import Allow, PolicyDocument, Principal, Statement

from stacker.blueprints.base import Blueprint
from stacker.blueprints.variables.types import CFNCommaDelimitedList, CFNString


CFN_ACTIONS = [awacs.cloudformation.CreateStack,
               awacs.cloudformation.DeleteStack,
               awacs.cloudformation.DescribeStacks,
               awacs.cloudformation.DescribeStackEvents,
               awacs.cloudformation.UpdateStack]


def get_str_class():
    """Return string class for the running version of python."""
    if sys.version_info[0] == 2:
        return basestring
    return str


class Iam(Blueprint):
    """Stacker blueprint for jenkins roles/policies."""

    VARIABLES = {
        'IamPermissions': {'type': get_str_class(),
                           'description': '(Optional) Comma-separated list '
                                          'of additional IAM permissions for '
                                          'the Jenkins server policy.',
                           'default': ''},
        'ConfigBucketArn': {'type': CFNString,
                            'description': '(Optional) Bucket containting '
                                           'Chef configs; will be used if the '
                                           '`chef_configs` IamPermission is '
                                           'granted.',
                            'default': ''},
        'DataBucketArn': {'type': CFNString,
                          'description': '(Optional) Bucket to which '
                                         'CodeBuild will be granted access.',
                          'default': ''},
        'EnvironmentName': {'type': CFNString,
                            'description': '(Optional) Name of Environment; '
                                           'used with DataBucketArn for '
                                           'CodeBuild',
                            'default': 'common'},
        'BucketKey': {'type': CFNString,
                      'description': '(Optional) S3 key prefix for use with '
                                     'DataBucketArn for CodeBuild',
                      'default': ''},
        'CodeBuildPolicies': {'type': CFNCommaDelimitedList,
                              'description': '(Optional) IAM managed policy '
                                             'ARNs to apply to the CodeBuild '
                                             'role',
                              'default': ''},
        'HostedZoneID': {'type': CFNString,
                         'description': 'Route53 hosted zone ID.',
                         'default': ''}
    }

    def create_template(self):
        """Create template (main function called by Stacker)."""
        template = self.template
        variables = self.get_variables()
        template.add_version('2010-09-09')
        template.add_description("Sturdy Platform - Jenkins - IAM "
                                 "- {0}".format(version()))

        # Conditions
        hostedzoneidspecified = 'HostedZoneIDSpecified'
        template.add_condition(
            hostedzoneidspecified,
            And(Not(Equals(variables['HostedZoneID'].ref, '')),
                Not(Equals(variables['HostedZoneID'].ref, 'undefined')))
        )

        databucketspecified = 'DataBucketSpecified'
        template.add_condition(
            databucketspecified,
            And(Not(Equals(variables['DataBucketArn'].ref, '')),
                Not(Equals(variables['DataBucketArn'].ref, 'undefined')),
                Not(Equals(variables['BucketKey'].ref, '')),
                Not(Equals(variables['BucketKey'].ref, 'undefined')))
        )

        codebuildpoliciesomitted = 'CodeBuildPoliciesOmitted'
        template.add_condition(
            codebuildpoliciesomitted,
            Equals(Join('', variables['CodeBuildPolicies'].ref), '')
        )

        # Resources
        codebuildrole = template.add_resource(
            iam.Role(
                'CodeBuildRole',
                Condition=databucketspecified,
                AssumeRolePolicyDocument=PolicyDocument(
                    Version='2012-10-17',
                    Statement=[
                        Statement(
                            Effect=Allow,
                            Action=[awacs.sts.AssumeRole],
                            Principal=Principal('Service',
                                                ['codebuild.amazonaws.com'])
                        )
                    ]
                ),
                ManagedPolicyArns=If(
                    codebuildpoliciesomitted,
                    Ref('AWS::NoValue'),
                    variables['CodeBuildPolicies'].ref
                ),
                Path='/',
                Policies=[
                    iam.Policy(
                        PolicyName='JenkinsCodeBuildPolicy',
                        PolicyDocument=PolicyDocument(
                            Version='2012-10-17',
                            Statement=[
                                Statement(
                                    Action=[
                                        awacs.logs.CreateLogGroup,
                                        awacs.logs.CreateLogStream,
                                        awacs.logs.PutLogEvents
                                    ],
                                    Effect=Allow,
                                    Resource=[
                                        Join('',
                                             ['arn:aws:logs:',
                                              Ref('AWS::Region'),
                                              ':',
                                              Ref('AWS::AccountId'),
                                              ':log-group:/aws/codebuild/'
                                              'jenkins*']),
                                        Join('',
                                             ['arn:aws:logs:',
                                              Ref('AWS::Region'),
                                              ':',
                                              Ref('AWS::AccountId'),
                                              ':log-group:/aws/codebuild/'
                                              'jenkins*:*'])]
                                ),
                                Statement(
                                    Action=[
                                        awacs.s3.PutObject,
                                        awacs.s3.GetObject,
                                        awacs.s3.GetObjectVersion
                                    ],
                                    Effect=Allow,
                                    Resource=[
                                        Join('',
                                             ['arn:aws:s3:::'
                                              'codepipeline-',
                                              Ref('AWS::Region'),
                                              '-*'])]
                                ),
                                Statement(
                                    Action=[
                                        awacs.ssm.GetParameter,
                                        awacs.ssm.GetParameters
                                    ],
                                    Effect=Allow,
                                    Resource=[
                                        Join('',
                                             ['arn:aws:ssm:',
                                              Ref('AWS::Region'),
                                              ':',
                                              Ref('AWS::AccountId'),
                                              ':parameter/CodeBuild/*'])]
                                ),
                                Statement(
                                    Action=[
                                        awacs.s3.PutObject,
                                        awacs.s3.GetObject,
                                        awacs.s3.GetObjectVersion
                                    ],
                                    Effect=Allow,
                                    Resource=[
                                        Join('',
                                             [variables['DataBucketArn'].ref,
                                              '/',
                                              variables['EnvironmentName'].ref,
                                              '/',
                                              variables['BucketKey'].ref,
                                              '/*'])]
                                )
                            ]
                        )
                    )
                ]
            )
        )
        template.add_output(
            Output(
                codebuildrole.title + 'Name',
                Condition=databucketspecified,
                Description='CodeBuild execution role name for Jenkins',
                Value=Ref(codebuildrole)
            )
        )
        template.add_output(
            Output(
                codebuildrole.title,
                Condition=databucketspecified,
                Description='CodeBuild execution role for Jenkins',
                Value=GetAtt(codebuildrole, 'Arn')
            )
        )

        # Dynamically build the CFN template with the specified permissions
        # (see README for `jenkins_iam_permissions`)
        jenkins_iam_statements = []
        if variables['IamPermissions'] not in ['', 'undefined']:
            requested_permissions = variables['IamPermissions'].split(',')
            actions_to_dedupe = []
            if 'stacker_bucket' in requested_permissions:
                jenkins_iam_statements.extend([
                    Statement(  # Stacker bucket management
                        Action=[
                            awacs.s3.CreateBucket,
                            awacs.aws.Action('s3', 'HeadBucket'),
                            awacs.s3.ListBucket
                        ],
                        Effect=Allow,
                        Resource=['arn:aws:s3:::stacker-*']
                    ),
                    Statement(  # Stacker template management
                        Action=[
                            awacs.s3.GetObject,
                            awacs.s3.PutObject
                        ],
                        Effect=Allow,
                        Resource=['arn:aws:s3:::stacker-*/*']
                    )
                ])
            if 'chef_configs' in requested_permissions:
                jenkins_iam_statements.append(
                    Statement(
                        Action=[
                            awacs.s3.PutObject
                        ],
                        Effect=Allow,
                        Resource=[
                            Join('', [variables['ConfigBucketArn'].ref,
                                      '/*'])
                        ]
                    )
                )
            if 'ssm' in requested_permissions:
                jenkins_iam_statements.append(
                    Statement(
                        Action=[
                            awacs.ssm.DeleteParameter,
                            awacs.aws.Action('ssm', 'GetParameter'),
                            awacs.ssm.GetParameters,
                            awacs.ssm.GetParameterHistory,
                            awacs.ssm.PutParameter
                        ],
                        Effect=Allow,
                        Resource=[
                            Join('', ['arn:aws:ssm:',
                                      Ref('AWS::Region'),
                                      ':',
                                      Ref('AWS::AccountId'),
                                      ':parameter/',
                                      variables['EnvironmentName'].ref,
                                      '.',
                                      variables['BucketKey'].ref,
                                      '.*']),
                            Join('', ['arn:aws:ssm:',
                                      Ref('AWS::Region'),
                                      ':',
                                      Ref('AWS::AccountId'),
                                      ':parameter/',
                                      variables['EnvironmentName'].ref,
                                      '/',
                                      variables['BucketKey'].ref,
                                      '/*'])
                        ]
                    )
                )
            if 'cloudformation' in requested_permissions:
                actions_to_dedupe.extend(CFN_ACTIONS)
            if 'packer' in requested_permissions:
                # https://www.packer.io/docs/builders/amazon.html#using-an-iam-task-or-instance-role
                actions_to_dedupe.extend([
                    awacs.ec2.AttachVolume,
                    awacs.ec2.AuthorizeSecurityGroupIngress,
                    awacs.ec2.CopyImage,
                    awacs.ec2.CreateImage,
                    awacs.ec2.CreateKeyPair,
                    awacs.ec2.CreateSecurityGroup,
                    awacs.ec2.CreateSnapshot,
                    awacs.ec2.CreateTags,
                    awacs.ec2.DeleteKeyPair,
                    awacs.ec2.CreateVolume,
                    awacs.ec2.DeleteSecurityGroup,
                    awacs.ec2.DeleteSnapshot,
                    awacs.ec2.DeleteVolume,
                    awacs.ec2.DeregisterImage,
                    awacs.ec2.DescribeImageAttribute,
                    awacs.ec2.DescribeImages,
                    awacs.ec2.DescribeInstances,
                    awacs.ec2.DescribeRegions,
                    awacs.ec2.DescribeSecurityGroups,
                    awacs.ec2.DescribeSnapshots,
                    awacs.ec2.DescribeSubnets,
                    awacs.ec2.DescribeTags,
                    awacs.ec2.DescribeVolumes,
                    awacs.ec2.DetachVolume,
                    awacs.ec2.GetPasswordData,
                    awacs.ec2.ModifyImageAttribute,
                    awacs.ec2.ModifyInstanceAttribute,
                    awacs.ec2.ModifySnapshotAttribute,
                    awacs.ec2.RegisterImage,
                    awacs.ec2.RunInstances,
                    awacs.ec2.StopInstances,
                    awacs.ec2.TerminateInstances
                ])
            if 'rds' in requested_permissions:
                actions_to_dedupe.extend(CFN_ACTIONS)
                actions_to_dedupe.extend([
                    awacs.aws.Action('rds', 'Describe*'),
                    awacs.ec2.AuthorizeSecurityGroupEgress,
                    awacs.ec2.AuthorizeSecurityGroupIngress,
                    awacs.ec2.CreateSecurityGroup,
                    awacs.ec2.DeleteSecurityGroup,
                    awacs.ec2.DescribeAccountAttributes,
                    awacs.ec2.DescribeSecurityGroups,
                    awacs.ec2.DescribeVpcs,
                    awacs.ec2.RevokeSecurityGroupEgress,
                    awacs.ec2.RevokeSecurityGroupIngress,
                    awacs.ec2.UpdateSecurityGroupRuleDescriptionsEgress,
                    awacs.ec2.UpdateSecurityGroupRuleDescriptionsIngress,
                    awacs.rds.AddTagsToResource,
                    awacs.rds.CreateDBInstance,
                    awacs.rds.CreateDBInstanceReadReplica,
                    awacs.rds.CreateDBSnapshot,
                    awacs.rds.DeleteDBInstance,
                    awacs.rds.ModifyDBInstance,
                    awacs.rds.RebootDBInstance,
                    awacs.rds.RestoreDBInstanceFromDBSnapshot
                ])
            if 'asg' in requested_permissions:
                actions_to_dedupe.extend(CFN_ACTIONS)
                actions_to_dedupe.extend([
                    awacs.autoscaling.CreateAutoScalingGroup,
                    awacs.autoscaling.CreateLaunchConfiguration,
                    awacs.autoscaling.DeleteAutoScalingGroup,
                    awacs.autoscaling.DeleteLaunchConfiguration,
                    awacs.autoscaling.DescribeAutoScalingGroups,
                    awacs.autoscaling.DescribeAutoScalingInstances,
                    awacs.autoscaling.DescribeLaunchConfigurations,
                    awacs.autoscaling.DescribeScalingActivities,
                    awacs.autoscaling.UpdateAutoScalingGroup,
                    awacs.awslambda.InvokeFunction,
                    awacs.iam.AddRoleToInstanceProfile,
                    awacs.iam.AttachRolePolicy,
                    awacs.iam.CreateInstanceProfile,
                    awacs.iam.CreatePolicy,
                    awacs.iam.CreateRole,
                    awacs.iam.DeleteInstanceProfile,
                    awacs.iam.DeletePolicy,
                    awacs.iam.DeleteRole,
                    awacs.iam.DeleteRolePolicy,
                    awacs.iam.DetachRolePolicy,
                    awacs.iam.GetPolicy,
                    awacs.iam.ListPolicyVersions,
                    awacs.iam.PassRole,
                    awacs.iam.PutRolePolicy,
                    awacs.iam.RemoveRoleFromInstanceProfile
                ])
            if actions_to_dedupe:
                jenkins_iam_statements.append(
                    Statement(
                        # Order preserving dedupe adapted from:
                        # https://stackoverflow.com/a/17016257
                        Action=list(OrderedDict.fromkeys(actions_to_dedupe)),
                        Effect=Allow,
                        Resource=['*']
                    )
                )

        # These IAM statements will be referenced via the CFN conditions
        hostedzone_stmnts = [
            # For aws_route53_record chef resource
            Statement(
                Action=[
                    awacs.route53.ListResourceRecordSets
                ],
                Effect=Allow,
                Resource=[
                    Join('',
                         ['arn:aws:route53:::hostedzone/',
                          variables['HostedZoneID'].ref])]
            ),
            # https://github.com/certbot/certbot/blob/v0.17.0/certbot-dns-route53/examples/sample-aws-policy.json
            Statement(
                Action=[awacs.route53.ListHostedZones,
                        awacs.route53.GetChange],
                Effect=Allow,
                Resource=['*']
            ),
            Statement(
                Action=[
                    awacs.route53.ChangeResourceRecordSets
                ],
                Effect=Allow,
                Resource=[
                    Join('',
                         ['arn:aws:route53:::hostedzone/',
                          variables['HostedZoneID'].ref])]
            )
        ]

        codebuild_stmnts = [
            Statement(
                Action=[awacs.iam.PassRole],
                Effect=Allow,
                Resource=[GetAtt(codebuildrole, 'Arn')]
            ),
            Statement(
                Action=[
                    awacs.aws.Action('codebuild', '*')
                ],
                Effect=Allow,
                Resource=[
                    Join('',
                         ['arn:aws:codebuild:',
                          Ref('AWS::Region'),
                          ':',
                          Ref('AWS::AccountId'),
                          ':project/jenkins*'])]
            ),
            Statement(
                Action=[
                    awacs.aws.Action('codebuild',
                                     'ListCuratedEnvironmentImages')
                ],
                Effect=Allow,
                Resource=['*']
            )
        ]

        serverpolicy = template.add_resource(
            iam.ManagedPolicy(
                'ServerPolicy',
                Description='Jenkins managed policy.',
                Path='/',
                PolicyDocument=If(
                    hostedzoneidspecified,
                    If(
                        databucketspecified,
                        PolicyDocument(
                            Version='2012-10-17',
                            Statement=(jenkins_iam_statements +
                                       hostedzone_stmnts +
                                       codebuild_stmnts)
                        ),
                        PolicyDocument(
                            Version='2012-10-17',
                            Statement=(jenkins_iam_statements +
                                       hostedzone_stmnts)
                        )
                    ),
                    If(
                        databucketspecified,
                        PolicyDocument(
                            Version='2012-10-17',
                            Statement=(jenkins_iam_statements +
                                       codebuild_stmnts)
                        ),
                        PolicyDocument(
                            Version='2012-10-17',
                            Statement=jenkins_iam_statements
                        )
                    )
                )
            )
        )
        template.add_output(
            Output(
                serverpolicy.title,
                Description='Jenkins server policy',
                Value=Ref(serverpolicy)
            )
        )


def version():
    """Call version function from top of repo."""
    root_dir = dirname(dirname(dirname(dirname(realpath(__file__)))))
    if root_dir not in sys.path:
        sys.path.append(root_dir)
    import platform_version  # pylint: disable=import-error
    return platform_version.version()


# Helper section to enable easy blueprint -> template generation
# (just run `python <thisfile>` to output the json)
if __name__ == "__main__":
    from stacker.context import Context

    print(Iam('test',
              Context({'namespace': 'test'}),
              None).to_json())
