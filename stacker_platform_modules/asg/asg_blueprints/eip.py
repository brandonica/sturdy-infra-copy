#!/usr/bin/env python
"""Stacker module for creating an Elastic IP ."""
from __future__ import print_function
from os.path import dirname, realpath
import sys

from troposphere import And, Equals, Not, Output, Ref, ec2, route53

from stacker.blueprints.base import Blueprint
from stacker.blueprints.variables.types import CFNString


class Eip(Blueprint):
    """Extends Stacker Blueprint class."""

    VARIABLES = {
        'HostedZoneID': {'type': CFNString,
                         'description': '(Optional) If associating a route53 '
                                        'entry with the ENI, specify the '
                                        'hosted zone ID.',
                         'default': ''},
        'Route53RecordName': {'type': CFNString,
                              'description': '(Optional) Specify a domain '
                                             'name to create route53 record '
                                             'for the address.',
                              'default': ''},
        'TTL': {'type': CFNString,
                'description': '(Optional) If associating a route53 '
                               'entry with the ENI, specify the '
                               'TTL.',
                'default': '600'}
    }

    def create_template(self):
        """Create template (main function called by Stacker)."""
        template = self.template
        variables = self.get_variables()
        template.add_version('2010-09-09')
        template.add_description("Sturdy Platform - ASG - Elastic IP "
                                 " - {0}".format(version()))

        # Conditions
        route53recordnamespecified = 'Route53RecordNameSpecified'
        template.add_condition(
            route53recordnamespecified,
            And(Not(Equals(variables['Route53RecordName'].ref, '')),
                Not(Equals(variables['Route53RecordName'].ref, 'undefined')))
        )

        # Resources
        elasticip = template.add_resource(
            ec2.EIP(
                'ElasticIP',
                Domain='vpc'
            )
        )
        template.add_output(
            Output(
                'PublicIp',
                Description='IP address',
                Value=Ref(elasticip)
            )
        )

        template.add_resource(
            route53.RecordSetType(
                'DNS',
                Condition=route53recordnamespecified,
                HostedZoneId=variables['HostedZoneID'].ref,
                Name=variables['Route53RecordName'].ref,
                ResourceRecords=[Ref(elasticip)],
                TTL=variables['TTL'].ref,
                Type='A'
            )
        )


def version():
    """Call version function from top of repo."""
    root_dir = dirname(dirname(dirname(dirname(realpath(__file__)))))
    if root_dir not in sys.path:
        sys.path.append(root_dir)
    import platform_version  # pylint: disable=import-error
    return platform_version.version()


# Helper section to enable easy blueprint -> template generation
# (just run `python <thisfile>` to output the json)
if __name__ == "__main__":
    from stacker.context import Context

    print(Eip('test',
              Context({'namespace': 'test'}),
              None).to_json())
