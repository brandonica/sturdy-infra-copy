"""Attaches an ENI to an instance."""
from __future__ import print_function

import boto3

ENIID = '{{NetworkInterface}}'
REGION = '{{AWS::Region}}'
INSTANCETAGKEY = '{{InstanceTagKey}}'
INSTANCETAGVALUE = '{{InstanceTagValue}}'


def handler(event, context):
    """Lambda entry point."""
    if event.get('ResourceProperties'):  # Invoked as CFN custom resource
        import cfnresponse  # pylint: disable=import-error
        # Bypass all execution when the 'resource' is being deleted
        if event['RequestType'] == 'Delete':
            cfnresponse.send(event, context, cfnresponse.SUCCESS, {}, False)
            return True
        instance_id = event['ResourceProperties']['InstanceId']
        device_index = event['ResourceProperties'].get('DeviceIndex', 1)
    else:  # invoked directly
        instance_id = event['InstanceId']
        device_index = event.get('DeviceIndex', 1)

    client = boto3.client('ec2', region_name=REGION)
    instance = client.describe_instances(
        InstanceIds=[instance_id]
    )['Reservations'][0]['Instances'][0]

    if INSTANCETAGKEY != '':
        if not any((d['Key'] == INSTANCETAGKEY and
                    d['Value'] == INSTANCETAGVALUE) for d in instance['Tags']):
            print('Invalid instance specified')
            if event.get('ResourceProperties'):
                cfnresponse.send(event, context, cfnresponse.FAILED, {}, False)
            return False
    int_status = client.describe_network_interfaces(
        NetworkInterfaceIds=[ENIID]
    )['NetworkInterfaces'][0]
    if (int_status.get('Attachment') and
            int_status['Attachment'].get('InstanceId')):
        if (int_status['Attachment']['InstanceId'] == instance_id and
                not (('detaching' in int_status['Attachment']['Status']) or
                     ('detached' in int_status['Attachment']['Status']))):
            # Already attached
            if event.get('ResourceProperties'):
                cfnresponse.send(
                    event, context, cfnresponse.SUCCESS, {}, False
                )
            return "already attached to %s" % instance_id

    attach_response = client.attach_network_interface(
        DeviceIndex=device_index,
        InstanceId=instance_id,
        NetworkInterfaceId=ENIID
    )

    if event.get('ResourceProperties'):
        cfnresponse.send(
            event, context, cfnresponse.SUCCESS, attach_response, False)
    return "attached to %s" % instance_id
