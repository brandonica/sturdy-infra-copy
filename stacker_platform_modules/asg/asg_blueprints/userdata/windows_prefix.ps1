<powershell>
Write-Host "Installing Chef"
. { iwr -TimeoutSec 10 -useb https://omnitruck.chef.io/install.ps1 } | iex; install -version {{ChefClientVersion}}
$directories = @(
  'C:\chef',
  'C:\chef\data_bags',
  'C:\chef\environments',
  'C:\chef\nodes',
  'C:\chef\roles',
  'C:\chef\logs'
)
Foreach ($directory in $directories)
{
  If (-not $(Test-Path $directory)) {
    New-Item -type directory -path $directory
    $Acl = Get-Acl $directory
    $Ar = New-Object  system.security.accesscontrol.filesystemaccessrule("Administrator","FullControl","Allow")
    $Acl.SetAccessRule($Ar)
    Set-Acl $directory $Acl
  }
}

Set-Content -Path C:\chef\sync_cookbooks.ps1 -Value @"
Read-S3Object -BucketName {{ChefBucketName}} -Region {{AWS::Region}} -Folder C:\chef -KeyPrefix {{EnvironmentName}}/{{BucketKey}}/
If( Test-Path C:\chef\cookbooks-*.tar.gz ) {
  Write-Host "Cookbook archive found"
  If ( Test-Path -PathType Container -Path C:\chef\cookbooks ) {
    Write-Host "Removing previously extracted cookbooks"
    Remove-Item -Recurse C:\chef\cookbooks
  }
  `$cbarchives = (ls C:\chef\cookbooks-*.tar.gz)
  C:\opscode\chef\bin\tar.exe -zxf `$cbarchives[-1].FullName -C C:\chef
}
"@

Set-Content -Path C:\chef\client.rb -Value @"
log_level :info
log_location 'C:\chef\logs\client.log'
ssl_verify_mode :verify_none
cookbook_path 'C:\chef\cookbooks'
node_path 'C:\chef\nodes'
role_path 'C:\chef\roles'
data_bag_path 'C:\chef\data_bags'
environment_path 'C:\chef\environments'
local_mode 'true'
"@

Set-Content -Path C:\chef\perform_chef_run.ps1 -Value @"
cd C:\chef
C:\opscode\chef\bin\chef-client.bat -z -r '{{ChefRunList}}' -c C:\chef\client.rb -E {{EnvironmentName}} --force-formatter --no-color -F min
"@
