#!/usr/bin/env python
"""Stacker module for creating a lambda function for looking up AMIs."""
from __future__ import print_function
from os import path
from os.path import dirname, realpath
import sys

from troposphere import (
    AWSHelperFn, And, Equals, GetAtt, If, Join, Not, Output, Ref, Tags,
    awslambda, ec2, iam, route53
)

import awacs.awslambda
import awacs.ec2
import awacs.sts
from awacs.aws import Allow, PolicyDocument, Principal, Statement

from stacker.lookups.handlers.file import parameterized_codec
from stacker.blueprints.base import Blueprint
from stacker.blueprints.variables.types import (
    CFNString, EC2SecurityGroupIdList, EC2SubnetId
)

AWS_LAMBDA_DIR = path.join(path.dirname(path.realpath(__file__)),
                           'aws_lambda')
IAM_ARN_PREFIX = 'arn:aws:iam::aws:policy/service-role/'


class EniAttach(Blueprint):
    """Extends Stacker Blueprint class."""

    eni_attach_src = parameterized_codec(
        open(path.join(AWS_LAMBDA_DIR, 'eni_attach.py'), 'r').read(),
        False  # disable base64 encoding
    )

    VARIABLES = {
        'ENIAttachLambdaFunction': {'type': AWSHelperFn,
                                    'description': 'Lambda function code',
                                    'default': eni_attach_src},
        'CustomerName': {'type': CFNString,
                         'description': 'The nickname for the new customer. '
                                        'Must be all lowercase letters, '
                                        'should not contain spaces or special '
                                        'characters, nor should it include '
                                        'any part of EnvironmentName.',
                         'allowed_pattern': '[-_ a-z]*',
                         'default': ''},
        'EnvironmentName': {'type': CFNString,
                            'description': 'Name of Environment',
                            'default': 'common'},
        'ApplicationName': {'type': CFNString,
                            'description': 'Application name (for tags)',
                            'default': 'application'},
        'ENIDescription': {'type': CFNString,
                           'description': '(Optional) ENI description.',
                           'default': ''},
        'HostedZoneID': {'type': CFNString,
                         'description': '(Optional) If associating a route53 '
                                        'entry with the ENI, specify the '
                                        'hosted zone ID.',
                         'default': ''},
        'Route53RecordName': {'type': CFNString,
                              'description': '(Optional) Specify a domain '
                                             'name to create route53 record '
                                             'for the address.',
                              'default': ''},
        'TTL': {'type': CFNString,
                'description': '(Optional) If associating a route53 '
                               'entry with the ENI, specify the '
                               'TTL.',
                'default': '600'},
        'InstanceTagKey': {'type': CFNString,
                           'description': '(Optional) Specify a tag key to '
                                          'have the Lambda function validate '
                                          'that the requested instance has '
                                          'this tag matching the value '
                                          'specified as "InstanceTagValue".',
                           'default': ''},
        'InstanceTagValue': {'type': CFNString,
                             'description': '(Optional) When "InstanceTagKey" '
                                            'is specified, provide the value '
                                            'of the tag here.',
                             'default': ''},
        'PrivateIpAddress': {'type': CFNString,
                             'description': '(Optional) Private IPv4 address '
                                            'to assign to the interface.',
                             'default': ''},
        'SecurityGroups': {'type': EC2SecurityGroupIdList,
                           'description': 'Security groups to apply to the '
                                          'interface',
                           'default': ''},
        'EnableSourceDestCheck': {'type': CFNString,
                                  'description': 'Should the ENI\'s source/'
                                                 'destination check be '
                                                 'enabled?',
                                  'default': 'true',
                                  'allowed_values': [
                                      'true',
                                      'false'
                                  ]},
        'SubnetId': {'type': EC2SubnetId,
                     'description': 'Subnet in which to deploy the ENI.'}
    }

    def create_template(self):
        """Create template (main function called by Stacker)."""
        template = self.template
        variables = self.get_variables()
        template.add_version('2010-09-09')
        template.add_description("Sturdy Platform - ASG - Elastic Network "
                                 "Interface - {0}".format(version()))

        # Conditions
        route53recordnamespecified = 'Route53RecordNameSpecified'
        template.add_condition(
            route53recordnamespecified,
            And(Not(Equals(variables['Route53RecordName'].ref, '')),
                Not(Equals(variables['Route53RecordName'].ref, 'undefined')))
        )
        enidescriptionomitted = 'ENIDescriptionOmitted'
        template.add_condition(
            enidescriptionomitted,
            Equals(variables['ENIDescription'].ref, '')
        )
        privateipaddressomitted = 'PrivateIpAddressOmitted'
        template.add_condition(
            privateipaddressomitted,
            Equals(variables['PrivateIpAddress'].ref, '')
        )
        securitygroupsomitted = 'SecurityGroupsOmitted'
        template.add_condition(
            securitygroupsomitted,
            Equals(Join('', variables['SecurityGroups'].ref), '')
        )
        sourcedestcheckenabled = 'SourceDestCheckEnabled'
        template.add_condition(
            sourcedestcheckenabled,
            Equals(variables['EnableSourceDestCheck'].ref, 'true')
        )

        # Resources
        networkinterface = template.add_resource(
            ec2.NetworkInterface(
                'NetworkInterface',
                Description=If(
                    enidescriptionomitted,
                    Ref('AWS::NoValue'),
                    variables['ENIDescription'].ref
                ),
                GroupSet=If(
                    securitygroupsomitted,
                    Ref('AWS::NoValue'),
                    variables['SecurityGroups'].ref
                ),
                PrivateIpAddress=If(
                    privateipaddressomitted,
                    Ref('AWS::NoValue'),
                    variables['PrivateIpAddress'].ref
                ),
                SourceDestCheck=If(
                    sourcedestcheckenabled,
                    True,
                    False
                ),
                SubnetId=variables['SubnetId'].ref,
                Tags=Tags(
                    application=variables['ApplicationName'].ref,
                    customer=variables['CustomerName'].ref,
                    environment=variables['EnvironmentName'].ref
                )
            )
        )
        template.add_output(
            Output(
                'Id',
                Description='Network interface ID',
                Value=Ref(networkinterface)
            )
        )
        template.add_output(
            Output(
                'PrimaryPrivateIpAddress',
                Description='Network interface private address',
                Value=GetAtt(networkinterface, 'PrimaryPrivateIpAddress')
            )
        )

        template.add_resource(
            route53.RecordSetType(
                'DNS',
                Condition=route53recordnamespecified,
                HostedZoneId=variables['HostedZoneID'].ref,
                Name=variables['Route53RecordName'].ref,
                ResourceRecords=[GetAtt(networkinterface,
                                        'PrimaryPrivateIpAddress')],
                TTL=variables['TTL'].ref,
                Type='A'
            )
        )

        eniattachlambdarole = template.add_resource(
            iam.Role(
                'ENIAttachLambdaRole',
                AssumeRolePolicyDocument=PolicyDocument(
                    Version='2012-10-17',
                    Statement=[
                        Statement(
                            Effect=Allow,
                            Action=[awacs.sts.AssumeRole],
                            Principal=Principal('Service',
                                                ['lambda.amazonaws.com'])
                        )
                    ]
                ),
                ManagedPolicyArns=[
                    IAM_ARN_PREFIX + 'AWSLambdaBasicExecutionRole'
                ],
                Policies=[
                    iam.Policy(
                        PolicyName=Join('-', ['eniattach-lambda-role',
                                              variables['EnvironmentName'].ref,
                                              variables['CustomerName'].ref]),
                        PolicyDocument=PolicyDocument(
                            Version='2012-10-17',
                            Statement=[
                                # None of these actions can be restricted by
                                # resource or condition
                                # https://docs.aws.amazon.com/AWSEC2/latest/APIReference/ec2-api-permissions.html
                                Statement(
                                    Action=[
                                        awacs.ec2.AttachNetworkInterface,
                                        awacs.ec2.DescribeInstances,
                                        awacs.ec2.DescribeNetworkInterfaces],
                                    Effect=Allow,
                                    Resource=['*']
                                )
                            ]
                        )
                    )
                ]
            )
        )

        eniattach = template.add_resource(
            awslambda.Function(
                'ENIAttach',
                Description='Attach ENI to instance',
                Code=awslambda.Code(
                    ZipFile=variables['ENIAttachLambdaFunction']
                ),
                Handler='index.handler',
                Role=GetAtt(eniattachlambdarole, 'Arn'),
                Runtime='python2.7',
                Timeout=15
            )
        )
        template.add_output(
            Output(
                'ENIAttachFunctionName',
                Description='ENI attachment function name',
                Value=Ref(eniattach)
            )
        )
        template.add_output(
            Output(
                'ENIAttachFunctionArn',
                Description='ENI attachment function Arn',
                Value=GetAtt(eniattach, 'Arn')
            )
        )

        # IAM Managed Policy granting instances permission to invoke the
        # function
        eniattachaccesspolicy = template.add_resource(
            iam.ManagedPolicy(
                'ENIAttachAccessPolicy',
                Description='Allows invocation of the ENI attach lambda '
                            'function.',
                Path='/',
                PolicyDocument=PolicyDocument(
                    Version='2012-10-17',
                    Statement=[
                        Statement(
                            Action=[awacs.awslambda.InvokeFunction],
                            Effect=Allow,
                            Resource=[GetAtt(eniattach, 'Arn')]
                        )
                    ]
                )
            )
        )
        template.add_output(
            Output(
                'InvokePolicy',
                Description='Policy allowing use of the ENI attachment lambda '
                            'function',
                Value=Ref(eniattachaccesspolicy)
            )
        )


def version():
    """Call version function from top of repo."""
    root_dir = dirname(dirname(dirname(dirname(realpath(__file__)))))
    if root_dir not in sys.path:
        sys.path.append(root_dir)
    import platform_version  # pylint: disable=import-error
    return platform_version.version()


# Helper section to enable easy blueprint -> template generation
# (just run `python <thisfile>` to output the json)
if __name__ == "__main__":
    from stacker.context import Context

    print(EniAttach('test',
                    Context({'namespace': 'test'}),
                    None).to_json())
