#!/usr/bin/env python
"""Stacker module for creating a standalone ASG."""
from __future__ import print_function
from collections import OrderedDict
import copy
from os import path
from os.path import dirname, realpath
import sys

from troposphere import (
    AWSHelperFn, And, Equals, GenericHelperFn, GetAtt, If, Join, Not, Output,
    Ref, autoscaling, cloudformation, ec2, iam
)
from troposphere.autoscaling import MetricsCollection

import awacs.ec2
import awacs.s3
import awacs.ssm
import awacs.sts
from awacs.aws import Allow, Condition, PolicyDocument, Principal, Statement
# Linter is incorrectly flagging the automatically generated functions in awacs
from awacs.aws import StringEquals, StringLike  # noqa pylint: disable=no-name-in-module

from stacker.blueprints.base import Blueprint, resolve_variable
from stacker.variables import Variable
from stacker.blueprints.variables.types import (
    CFNCommaDelimitedList, CFNNumber, CFNString, EC2SecurityGroupIdList,
    EC2SubnetIdList
)
from stacker.lookups.handlers.file import parameterized_codec


def get_str_class():
    """Return string class for the running version of python."""
    if sys.version_info[0] == 2:
        return basestring
    return str


class AMIId(cloudformation.AWSCustomObject):
    """Class for AMI lookup custom resource."""

    resource_type = "Custom::AMIId"

    props = {
        'ServiceToken': (get_str_class(), True),
        'Platform': (get_str_class(), True),
        'Region': (get_str_class(), True)
    }


class AppServer(Blueprint):
    """Extends Stacker Blueprint class."""

    with open(path.join(path.dirname(path.realpath(__file__)),
                        'userdata/amazonlinux_prefix.yaml')) as read_file:
        amazon_linux_userdata = GenericHelperFn(read_file.read())

    VARIABLES = {
        'UserDataPrefix': {'type': AWSHelperFn,
                           'description': 'AMI ID for app instances',
                           'default': amazon_linux_userdata},
        'AppOS': {'type': get_str_class(),
                  'description': 'Instance OS type; determines userdata '
                                 'provided to instances, and the AMI to use '
                                 'if "AppAMI" isn\'t provided. Set to "custom"'
                                 ' to provide a custom UserDataPrefix.',
                  'default': 'amazon-hvm64'},
        'AppAMI': {'type': CFNString,  # Not EC2ImageId to allow blank value
                   'description': 'AMI ID for app instances; leave blank to '
                                  'automatically look up the via '
                                  '"AMILookupArn".',
                   'default': ''},
        'AMILookupArn': {'type': CFNString,
                         'description': 'Lambda function to query for AMI Ids;'
                                        ' used when "AppAMI" is omitted.',
                         'default': ''},
        'AppInstanceType': {'type': CFNString,
                            'description': 'Type of the management '
                                           'instances',
                            'default': 'm3.medium'},
        'AppPolicies': {'type': CFNCommaDelimitedList,
                        'description': 'IAM managed policy ARNs to apply to '
                                       'the instances'},
        'AppSecurityGroups': {'type': EC2SecurityGroupIdList,
                              'description': 'Security groups to apply to the '
                                             'instances'},
        'AppSubnets': {'type': EC2SubnetIdList,
                       'description': 'Subnets in which the app server(s) '
                                      'will be deployed'},
        'ApplicationName': {'type': CFNString,
                            'description': 'Instance name tag value (will have'
                                           ' "CustomerName" prepended and '
                                           '"EnvironmentName" appended to it)',
                            'default': 'application'},
        'ASGMinValue': {'type': CFNString,
                        'description': 'Minimum number of instances that will '
                                       'be running in the autoscaling group',
                        'default': '1'},
        'ASGMaxValue': {'type': CFNString,
                        'description': 'Maximum number of instances that will '
                                       'be running in the autoscaling group',
                        'default': '1'},
        'AppVolumeName': {'type': CFNString,
                          'description': 'Instance EBS volume name',
                          'default': '/dev/xvda'},
        'AppVolumeSize': {'type': CFNNumber,
                          'description': 'Size of instance EBS volume in GB',
                          'default': '15'},
        'AppVolumeType': {'type': CFNString,
                          'description': 'Instance EBS volume type',
                          'allowed_values': [
                              'standard',
                              'io1',
                              'gp2'
                          ],
                          'default': 'gp2'},
        'AssociatePublicIpAddress': {'type': CFNString,
                                     'description': 'Should a public IP '
                                                    'be automatically '
                                                    'associated with launched '
                                                    'instances?',
                                     'default': 'false'},
        'BucketKey': {'type': CFNString,
                      'description': 'S3 prefix for chef cookbook archives '
                                     'and artifacts. The environment name '
                                     'will be appended to this. E.g. if '
                                     '"ChefBucketName" is "foo", '
                                     '"BucketKey" is "bar", '
                                     '"EnvironmentName" is "dev", and '
                                     '"ChefDataBucketName" is "citadel", '
                                     'instances will pull configuration '
                                     'tarballs from s3://foo/dev/bar/ and be '
                                     'able to access files (e.g. secrets/'
                                     'artifacts) in s3://citadel/dev/bar/',
                      'default': 'application'},
        'ChefAttributeKey': {'type': CFNString,
                             'description': '(optional) top level attribute '
                                            'under which any "Chef attribute '
                                            'to be populated..." parameter '
                                            'will be populated',
                             'default': 'application'},
        'ChefAttributeValues': {'type': dict,
                                'default': {}},
        'ChefBucketName': {'type': CFNString,
                           'description': 'Name of bucket storing Chef '
                                          'configuration archives',
                           'default': 'common'},
        'ChefClientVersion': {'type': CFNString,
                              'description': 'Version of chef-client to '
                                             'install',
                              'default': '13.2.20'},
        'ChefDataBucketArn': {'type': CFNString,
                              'description': '(Typically unused) ARN of '
                                             '"ChefDataBucketName". Specify '
                                             'this when the ARN should not be '
                                             'automatically generated (i.e. '
                                             'when running in an isolated '
                                             'region).',
                              'default': ''},
        'ChefDataBucketName': {'type': CFNString,
                               'description': 'Name of bucket storing extra '
                                              'Chef data and application '
                                              'artifacts'},
        'ChefRunList': {'type': CFNString,
                        'description': 'Recipes to execute',
                        'default': 'application::default'},
        # https://cloudinit.readthedocs.io/en/latest/topics/modules.html#package-update-upgrade-install
        'CloudInitPackageUpdate': {'type': CFNString,
                                   'description': 'Value for package_update/'
                                                  'repo_update (they\'re '
                                                  'synonymous) cloud-init '
                                                  'command',
                                   'default': 'true',
                                   'allowed_values': [
                                       'true',
                                       'false'
                                   ]},
        # https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AmazonLinuxAMIBasics.html#security-updates
        'CloudInitRepoReleaseVer': {'type': CFNString,
                                    'description': 'Value for repo_releasever '
                                                   '(Amazon Linux specific) '
                                                   'cloud-init command',
                                    'default': 'latest'},
        # https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AmazonLinuxAMIBasics.html#security-updates
        'CloudInitRepoUpgrade': {'type': CFNString,
                                 'description': 'Value for repo_upgrade '
                                                'cloud-init command',
                                 'default': 'security',
                                 'allowed_values': [
                                     'bugfix',
                                     'none',
                                     'security',
                                     'all',
                                     'true',  # for debian-family
                                     'false'  # for debian-family
                                 ]},
        'CustomerName': {'type': CFNString,
                         'description': 'The nickname for the customer/tenant.'
                                        ' Must be all lowercase letters, '
                                        'should not contain spaces or special '
                                        'characters, nor should it include '
                                        'any part of EnvironmentName',
                         'allowed_pattern': '[-_ a-z]*',
                         'default': ''},
        'ELBNames': {'type': CFNCommaDelimitedList,
                     'description': '(Optional) ELBs to associate with the '
                                    'app ASG.',
                     'default': ''},
        'EnvironmentName': {'type': CFNString,
                            'description': 'Name of Environment',
                            'default': 'common'},
        'HealthCheckGracePeriod': {'type': CFNNumber,
                                   'description': 'ASG health check grace '
                                                  'period (in seconds)',
                                   'default': '600'},
        'HealthCheckType': {'type': CFNString,
                            'description': 'Type of ASG health check',
                            'default': 'EC2',
                            'allowed_values': [
                                'EC2',
                                'ELB'
                            ]},
        # Not using EC2KeyPairKeyName to allow KeyName to be optional
        'KeyName': {'type': CFNString,
                    'description': 'Name of an existing EC2-VPC KeyPair',
                    'default': ''},
        'MetricsCollection': {'type': CFNString,
                              'description': 'Should the autoscaling have '
                                             'metrics?',
                              'default': 'false',
                              'allowed_values': ['true',
                                                 'false']},
        'MetricsCollectionGranularity': {'type': CFNString,
                                         'description': 'How often to pull '
                                                        'metrics',
                                         'default': '1Minute'},
        'TargetGroupARNs': {'type': CFNCommaDelimitedList,
                            'description': '(Optional) Target groups to '
                                           'associate with the app ASG.',
                            'default': ''},
        'OtherTags': {'type': dict,
                      'default': {}}
    }

    # This will be used in resolve_variables to translate the dictionaries
    # in the config file to CFN parameters in VARIABLES
    PARAMS_TO_ADD = [
        {'var_name': 'OtherTags',
         'var_type': CFNString,
         'description': 'Extra tag value to apply to the instances'},
        {'var_name': 'ChefAttributeValues',
         'var_type': CFNString,
         'description': 'Chef attribute to be populated under '
                        '"ChefAttributeKey"'}
    ]
    DEFINED_VARIABLES = {}

    def defined_variables(self):
        """Override the blueprint defined_variables function.

        Returns:
            dict: variables defined by the blueprint, including our injected
                dynamic variables

        """
        if self.DEFINED_VARIABLES == {}:
            return copy.deepcopy(getattr(self, "VARIABLES", {}))
        return self.DEFINED_VARIABLES

    def resolve_variables(self, provided_variables):
        """Override the blueprint resolve_variables function.

        This allows our injection of dynamic variables.
        """
        self.resolved_variables = {}
        variable_dict = dict((var.name, var) for var in provided_variables)
        variable_dict.update(update_var_dict(variable_dict,
                                             self.PARAMS_TO_ADD))
        # Disabling invalid-name check because we didn't choose the var name
        self.DEFINED_VARIABLES = updated_def_variables(  # noqa pylint: disable=C0103
            self.defined_variables(),
            variable_dict,
            self.PARAMS_TO_ADD
        )
        for var_name, var_def in self.DEFINED_VARIABLES.items():
            value = resolve_variable(
                var_name,
                var_def,
                variable_dict.get(var_name),
                self.name
            )
            self.resolved_variables[var_name] = value

    def create_template(self):  # pylint: disable=too-many-locals
        """Create template (main function called by Stacker)."""
        template = self.template
        variables = self.get_variables()
        template.add_version('2010-09-09')
        template.add_description("Sturdy Platform - ASG - Standalone "
                                 "- {0}".format(version()))

        # Conditions
        appamiomitted = 'AppAMIOmitted'
        template.add_condition(
            appamiomitted,
            Equals(variables['AppAMI'].ref, '')
        )

        chefdatabucketarnomitted = 'ChefDataBucketArnOmitted'
        template.add_condition(
            chefdatabucketarnomitted,
            Equals(variables['ChefDataBucketArn'].ref, '')
        )

        elbnamesomitted = 'ELBNamesOmitted'
        template.add_condition(
            elbnamesomitted,
            Equals(Join('', variables['ELBNames'].ref), '')
        )

        metricscollectionomitted = 'MetricsCollectionOmitted'
        template.add_condition(
            metricscollectionomitted,
            Equals(variables['MetricsCollection'].ref, 'false')
        )

        sshkeyspecified = 'SSHKeySpecified'
        template.add_condition(
            sshkeyspecified,
            And(Not(Equals(variables['KeyName'].ref, '')),
                Not(Equals(variables['KeyName'].ref, 'undefined')))
        )

        targetgrouparnsomitted = 'TargetGroupARNsOmitted'
        template.add_condition(
            targetgrouparnsomitted,
            Equals(Join('', variables['TargetGroupARNs'].ref), '')
        )

        # Resources
        appserverrole = template.add_resource(
            iam.Role(
                'AppServerRole',
                AssumeRolePolicyDocument=PolicyDocument(
                    Version='2012-10-17',
                    Statement=[
                        Statement(
                            Effect=Allow,
                            Action=[awacs.sts.AssumeRole],
                            Principal=Principal('Service',
                                                ['ec2.amazonaws.com'])
                        )
                    ]
                ),
                ManagedPolicyArns=variables['AppPolicies'].ref,
                Path='/',
                Policies=[
                    iam.Policy(
                        PolicyName=Join('-', [variables['ApplicationName'].ref,
                                              'role',
                                              variables['EnvironmentName'].ref,
                                              variables['CustomerName'].ref]),
                        PolicyDocument=PolicyDocument(
                            Version='2012-10-17',
                            Statement=[
                                Statement(
                                    Action=[awacs.aws.Action('s3', 'Get*'),
                                            awacs.aws.Action('s3', 'List*'),
                                            awacs.aws.Action('s3', 'Put*')],
                                    Effect=Allow,
                                    Resource=[
                                        Join(
                                            '',
                                            [If(chefdatabucketarnomitted,
                                                Join(
                                                    '',
                                                    ['arn:aws:s3:::',
                                                     variables['ChefDataBucketName'].ref]),  # noqa
                                                variables['ChefDataBucketArn'].ref),  # noqa
                                             '/',
                                             variables['EnvironmentName'].ref,
                                             '/',
                                             variables['BucketKey'].ref,
                                             '/*'])]
                                ),
                                Statement(
                                    Action=[awacs.s3.ListBucket],
                                    Effect=Allow,
                                    Resource=[
                                        Join('',
                                             [If(chefdatabucketarnomitted,
                                                 Join(
                                                     '',
                                                     ['arn:aws:s3:::',
                                                      variables['ChefDataBucketName'].ref],  # noqa
                                                 ),
                                                 variables['ChefDataBucketArn'].ref)])],  # noqa
                                    Condition=Condition(
                                        StringLike(
                                            's3:prefix',
                                            [Join(
                                                '',
                                                [variables['EnvironmentName'].ref,  # noqa
                                                 '/',
                                                 variables['BucketKey'].ref,
                                                 '/*'])])
                                    )
                                ),
                                Statement(
                                    Action=[awacs.ssm.GetParameters],
                                    Effect=Allow,
                                    Resource=[
                                        Join('',
                                             ['arn:aws:ssm:',
                                              Ref('AWS::Region'),
                                              ':',
                                              Ref('AWS::AccountId'),
                                              ':parameter/',
                                              variables['EnvironmentName'].ref,
                                              '.',
                                              variables['BucketKey'].ref,
                                              '.*']),
                                        Join('',
                                             ['arn:aws:ssm:',
                                              Ref('AWS::Region'),
                                              ':',
                                              Ref('AWS::AccountId'),
                                              ':parameter/',
                                              variables['EnvironmentName'].ref,
                                              '/',
                                              variables['BucketKey'].ref,
                                              '/*'])
                                    ]
                                ),
                                # Half of the necessary IAM permissions to
                                # attach volumes; combine with managed policies
                                # on this role granting permission to
                                # AttachVolume on the specific volume ARN(s)
                                Statement(
                                    Action=[awacs.ec2.AttachVolume],
                                    Effect=Allow,
                                    Resource=[
                                        Join('', ['arn:aws:ec2:',
                                                  Ref('AWS::Region'),
                                                  ':',
                                                  Ref('AWS::AccountId'),
                                                  ':instance/*'])
                                    ],
                                    Condition=Condition(
                                        StringEquals(
                                            'ec2:ResourceTag/'
                                            'aws:cloudformation:stack-name',
                                            Ref('AWS::StackName'))
                                    )
                                )
                            ]
                        )
                    )
                ]
            )
        )

        appinstanceprofile = template.add_resource(
            iam.InstanceProfile(
                'AppInstanceProfile',
                Path='/',
                Roles=[Ref(appserverrole)]
            )
        )

        amiid = template.add_resource(
            AMIId(
                'AMIId',
                Condition=appamiomitted,
                Platform=variables['AppOS'],
                Region=Ref('AWS::Region'),
                ServiceToken=variables['AMILookupArn'].ref
            )
        )

        if (variables['AppOS'].startswith('amazon') or
                variables['AppOS'].startswith('custom')):
            ud_prefix = variables['UserDataPrefix'].data
        elif variables['AppOS'].startswith('ubuntu'):
            with open(path.join(path.dirname(path.realpath(__file__)),
                                'userdata/ubuntu_prefix.yaml')) as read_file:
                ud_prefix = read_file.read()
        if variables['AppOS'].startswith('windows'):
            with open(path.join(path.dirname(path.realpath(__file__)),
                                'userdata/windows_prefix.ps1')) as read_file:
                ud_prefix = read_file.read()
            ud_suffix = windows_userdata_suffix(variables)
        else:
            ud_suffix = generate_userdata_suffix(variables)

        appserverlaunchconfig = template.add_resource(
            autoscaling.LaunchConfiguration(
                'AppServerLaunchConfig',
                AssociatePublicIpAddress=variables[('AssociatePublic'
                                                    'IpAddress')].ref,
                BlockDeviceMappings=[
                    ec2.BlockDeviceMapping(
                        DeviceName=variables['AppVolumeName'].ref,
                        Ebs=ec2.EBSBlockDevice(
                            # Most AMIs default to deleteonterminate but it
                            # doesn't hurt to specify it just in case (i.e. for
                            # CentOS)
                            DeleteOnTermination=True,
                            VolumeSize=variables['AppVolumeSize'].ref,
                            VolumeType=variables['AppVolumeType'].ref
                        )
                    )
                ],
                IamInstanceProfile=Ref(appinstanceprofile),
                ImageId=If(appamiomitted,
                           GetAtt(amiid, 'ImageId'),
                           variables['AppAMI'].ref),
                InstanceType=variables['AppInstanceType'].ref,
                InstanceMonitoring=False,  # extra granularity not worth cost
                KeyName=If(sshkeyspecified,
                           variables['KeyName'].ref,
                           Ref('AWS::NoValue')),
                SecurityGroups=variables['AppSecurityGroups'].ref,
                UserData=parameterized_codec(
                    ud_prefix + ud_suffix,
                    True)  # wrap in Base64
            )
        )

        additional_tags = []
        for key in variables['OtherTags']:
            if isinstance(variables['OtherTags'][key], dict):
                tag_name = variables['OtherTags'][key]['Name']
            else:
                tag_name = key
            additional_tags.append(autoscaling.Tag(tag_name,
                                                   variables[key].ref,
                                                   True))

        appserverasg = template.add_resource(
            autoscaling.AutoScalingGroup(
                'AppServerASG',
                MinSize=variables['ASGMinValue'].ref,
                MaxSize=variables['ASGMaxValue'].ref,
                HealthCheckGracePeriod=variables['HealthCheckGracePeriod'].ref,
                HealthCheckType=variables['HealthCheckType'].ref,
                MetricsCollection=If(
                    metricscollectionomitted,
                    Ref('AWS::NoValue'),
                    [MetricsCollection(
                        Granularity=variables['MetricsCollectionGranularity'].ref  # noqa
                    )]
                ),
                LaunchConfigurationName=Ref(appserverlaunchconfig),
                LoadBalancerNames=If(elbnamesomitted,
                                     Ref('AWS::NoValue'),
                                     variables['ELBNames'].ref),
                Tags=[
                    autoscaling.Tag('Name',
                                    Join('-',
                                         [variables['CustomerName'].ref,
                                          variables['ApplicationName'].ref,
                                          variables['EnvironmentName'].ref]),
                                    True),
                    autoscaling.Tag('environment',
                                    variables['EnvironmentName'].ref,
                                    True),
                    autoscaling.Tag('customer',
                                    variables['CustomerName'].ref,
                                    True)
                ] + additional_tags,
                TargetGroupARNs=If(targetgrouparnsomitted,
                                   Ref('AWS::NoValue'),
                                   variables['TargetGroupARNs'].ref),
                VPCZoneIdentifier=variables['AppSubnets'].ref
            )
        )
        template.add_output(Output(
            appserverasg.title,
            Description='Name of autoscaling group',
            Value=Ref(appserverasg),
        ))


def generate_userdata_suffix(variables):
    """Generate Chef environment file for userdata.

    Returns: String to be appended to userdata.

    """
    userdata_suffix = [
        "  - path: /etc/chef/environments/{{EnvironmentName}}.json\n",
        "    permissions: '0644'\n",
        "    owner: 'root'\n",
        "    group: 'root'\n",
        "    content: |\n",
        "      {\n",
        "        \"name\": \"{{EnvironmentName}}\",\n",
        "        \"json_class\": \"Chef::Environment\",\n",
        "        \"description\": \"{{EnvironmentName}} environment\",\n",
        "        \"chef_type\": \"environment\",\n",
        "        \"default_attributes\": {\n",
        "          \"citadel\": {\n",
        "            \"bucket\": \"{{ChefDataBucketName}}\",\n",
        "            \"region\": \"{{AWS::Region}}\"\n",
        "          }"
    ]
    # If at least one attribute is specified, construct the json dict
    if bool(variables['ChefAttributeValues']):
        userdata_suffix.extend([
            ",\n",
            "          \"{{ChefAttributeKey}}\": {\n"
        ])

        # Add all but the last attribute to the json dict
        for key in sorted(variables['ChefAttributeValues'])[:-1]:
            userdata_suffix.extend([
                "            \"%s\": \"{{%s}}\",\n" % (key, key)
            ])

        # Add the last item (without a trailing comma) and close the
        # top level attribute
        last_key = sorted(variables['ChefAttributeValues'])[-1]
        userdata_suffix.extend([
            "            \"%s\": \"{{%s}}\"\n" % (last_key, last_key),
            "          }"
        ])

    # Close out json file
    userdata_suffix.extend([
        "\n",
        "        }\n",
        "      }\n",
        "\n"
    ])

    return ''.join(userdata_suffix)


def windows_userdata_suffix(variables, include_cfn_signal=False):
    """Generate Chef environment file for Windows userdata.

    Returns: String to be appended to userdata.

    """
    userdata_suffix = [
        "Set-Content -Path C:\\chef\\environments\\{{EnvironmentName}}.json",
        " -Value @\"\n",
        "{\n",
        "  \"name\": \"{{EnvironmentName}}\",\n",
        "  \"json_class\": \"Chef::Environment\",\n",
        "  \"description\": \"{{EnvironmentName}} environment\",\n",
        "  \"chef_type\": \"environment\",\n",
        "  \"default_attributes\": {\n",
        "    \"citadel\": {\n",
        "      \"bucket\": \"{{ChefDataBucketName}}\",\n",
        "      \"region\": \"{{AWS::Region}}\"\n",
        "    }"
    ]
    # If at least one attribute is specified, construct the json dict
    if bool(variables['ChefAttributeValues']):
        userdata_suffix.extend([
            ",\n",
            "    \"{{ChefAttributeKey}}\": {\n"
        ])

        # Add all but the last attribute to the json dict
        for key in sorted(variables['ChefAttributeValues'])[:-1]:
            userdata_suffix.extend([
                "      \"%s\": \"{{%s}}\",\n" % (key, key)
            ])

        # Add the last item (without a trailing comma) and close the
        # top level attribute
        last_key = sorted(variables['ChefAttributeValues'])[-1]
        userdata_suffix.extend([
            "      \"%s\": \"{{%s}}\"\n" % (last_key, last_key),
            "    }"
        ])

    # Close out json file
    if include_cfn_signal:
        userdata_suffix.extend([
            "\n",
            "  }\n",
            "}\n",
            "\"@\n\n",
            "$policy=(Get-ExecutionPolicy)\n",
            "Set-ExecutionPolicy Unrestricted -Force\n",
            "try {\n",
            "  C:\\chef\\sync_cookbooks.ps1\n",
            "  If ($LASTEXITCODE -eq 0) {\n",
            "    C:\\chef\\perform_chef_run.ps1\n",
            "    cfn-signal.exe --resource AppServerASG ",
            "--exit-code $LASTEXITCODE --stack {{AWS::StackId}} ",
            "--region {{AWS::Region}}\n",
            "  } else {\n",
            "    cfn-signal.exe --resource AppServerASG --success false ",
            "--stack {{AWS::StackId}} --region {{AWS::Region}}\n",
            "  }\n",
            "} finally {\n",
            "  Set-ExecutionPolicy $policy -Force\n",
            "}\n",
            "</powershell>"
        ])
    else:
        userdata_suffix.extend([
            "\n",
            "  }\n",
            "}\n",
            "\"@\n\n",
            "$policy=(Get-ExecutionPolicy)\n",
            "Set-ExecutionPolicy Unrestricted -Force\n",
            "try {\n",
            "  C:\\chef\\sync_cookbooks.ps1\n",
            "  If ($LASTEXITCODE -eq 0) {\n",
            "    C:\\chef\\perform_chef_run.ps1\n",
            "  }\n",
            "} finally {\n",
            "  Set-ExecutionPolicy $policy -Force\n",
            "}\n",
            "</powershell>"
        ])

    return ''.join(userdata_suffix)


def updated_def_variables(variables, provided_var_dict, params_to_add):
    """Add CFN parameters to template based on the specified lists.

    Example params_to_add list:
        params_to_add = [
            {'var_name': 'OtherTags',
             'var_type': CFNString,
             'description': 'Extra tag value to apply to the instances'},
            {'var_name': 'OtherSGs',
             'var_type': CFNString,
             'description': 'Extra security group to apply to the instances'}
        ]
    """
    for param_to_add in params_to_add:
        if param_to_add['var_name'] in provided_var_dict:
            for key, _value in provided_var_dict[param_to_add['var_name']].value.items():  # noqa pylint: disable=C0301
                variables[key] = {
                    'type': param_to_add['var_type'],
                    'description': param_to_add['description']
                }
    return variables


def update_var_dict(provided_var_dict, params_to_add):
    """Return a dictionary to add to resolve_variables()'s variable_dict."""
    additional_vars = {}
    for param_to_add in params_to_add:
        if param_to_add['var_name'] in provided_var_dict:
            for key, value in provided_var_dict[param_to_add['var_name']].value.items():  # noqa pylint: disable=C0301
                if isinstance(value, (dict, OrderedDict)):
                    additional_vars[key] = Variable(key, dict(value)['Value'])
                else:
                    additional_vars[key] = Variable(key, value)
    return additional_vars


def version():
    """Call version function from top of repo."""
    root_dir = dirname(dirname(dirname(dirname(realpath(__file__)))))
    if root_dir not in sys.path:
        sys.path.append(root_dir)
    import platform_version  # pylint: disable=import-error
    return platform_version.version()


# Helper section to enable easy blueprint -> template generation
# (just run `python <thisfile>` to output the json)
if __name__ == "__main__":
    from stacker.context import Context

    print(AppServer('test',
                    Context({'namespace': 'test'}),
                    None).to_json())
