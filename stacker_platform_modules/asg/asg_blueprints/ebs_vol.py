#!/usr/bin/env python
"""Module with ASG EBS volume."""
from __future__ import print_function
from collections import OrderedDict
import copy
from os.path import dirname, realpath
import sys

from troposphere import Equals, If, Join, Ref, Output, Tags, ec2, iam

import awacs.ec2
from awacs.aws import Allow, PolicyDocument, Statement

from stacker.blueprints.base import Blueprint, resolve_variable
from stacker.blueprints.variables.types import CFNNumber, CFNString
from stacker.variables import Variable


class EbsVol(Blueprint):
    """Blueprint for creating an EBS volume."""

    VARIABLES = {
        'ApplicationName': {'type': CFNString,
                            'description': 'Application name (for tags)',
                            'default': 'application'},
        'CustomerName': {'type': CFNString,
                         'description': 'The nickname for the customer/tenant.'
                                        ' Must be all lowercase letters, '
                                        'should not contain spaces or special '
                                        'characters, nor should it include '
                                        'any part of EnvironmentName',
                         'allowed_pattern': '[-_ a-z]*',
                         'default': ''},
        'EnvironmentName': {'type': CFNString,
                            'description': 'Name of Environment',
                            'default': 'common'},
        'AvailabilityZone': {'type': CFNString,
                             'description': 'AZ in which to create the '
                                            'volume.'},
        'KmsKeyId': {'type': CFNString,
                     'description': '(Optional) ARN of the KMS master key '
                                    'used to create the encrypted volume.',
                     'default': ''},
        'SnapshotId': {'type': CFNString,
                       'description': '(Optional) snapshot id from which to '
                                      'create the volume.',
                       'default': ''},
        'Iops': {'type': CFNNumber,
                 'description': '(Only used with io1 volumes) number of '
                                'I/O operations per second that the '
                                'volume supports.',
                 'default': '100'},
        'Size': {'type': CFNNumber,
                 'description': 'Size (in in gibibytes) of volume. Leave at 0 '
                                'when specifying a "SnapshotId" to use the '
                                'size of the snapshot.',
                 'default': '0'},
        'VolumeType': {'type': CFNString,
                       'description': 'EBS volume type.',
                       'allowed_values': [
                           'standard',
                           'io1',
                           'gp2',
                           'sc1',
                           'st1'
                       ],
                       'default': 'gp2'},
        'OtherTags': {'type': dict,
                      'default': {}}
    }

    # This will be used in resolve_variables to translate the dictionaries
    # in the config file to CFN parameters in VARIABLES
    PARAMS_TO_ADD = [
        {'var_name': 'OtherTags',
         'var_type': CFNString,
         'description': 'Extra tag value to apply to the volume'}
    ]
    DEFINED_VARIABLES = {}

    def defined_variables(self):
        """Override the blueprint defined_variables function.

        Returns:
            dict: variables defined by the blueprint, including our injected
                dynamic variables

        """
        if self.DEFINED_VARIABLES == {}:
            return copy.deepcopy(getattr(self, "VARIABLES", {}))
        return self.DEFINED_VARIABLES

    def resolve_variables(self, provided_variables):
        """Override the blueprint resolve_variables function.

        This allows our injection of dynamic variables.
        """
        self.resolved_variables = {}
        variable_dict = dict((var.name, var) for var in provided_variables)
        variable_dict.update(update_var_dict(variable_dict,
                                             self.PARAMS_TO_ADD))
        # Disabling invalid-name check because we didn't choose the var name
        self.DEFINED_VARIABLES = updated_def_variables(  # noqa pylint: disable=C0103
            self.defined_variables(),
            variable_dict,
            self.PARAMS_TO_ADD
        )
        for var_name, var_def in self.DEFINED_VARIABLES.items():
            value = resolve_variable(
                var_name,
                var_def,
                variable_dict.get(var_name),
                self.name
            )
            self.resolved_variables[var_name] = value

    def create_template(self):
        """Create template (main function called by Stacker)."""
        template = self.template
        variables = self.get_variables()
        template.add_version('2010-09-09')
        template.add_description("Sturdy Platform - ASG - EBS Volume "
                                 "- {0}".format(version()))

        # Conditions
        kmskeyidomitted = 'KmsKeyIdOmitted'
        template.add_condition(
            kmskeyidomitted,
            Equals(variables['KmsKeyId'].ref, '')
        )

        sizeomitted = 'SizeOmitted'
        template.add_condition(
            sizeomitted,
            Equals(variables['Size'].ref, '0')
        )

        snapshotidomitted = 'SnapshotIdOmitted'
        template.add_condition(
            snapshotidomitted,
            Equals(variables['SnapshotId'].ref, '')
        )

        io1voltype = 'Io1VolType'
        template.add_condition(
            io1voltype,
            Equals(variables['VolumeType'].ref, 'io1')
        )

        # Resources
        additional_tags = {}
        for key in variables['OtherTags']:
            if isinstance(variables['OtherTags'][key], dict):
                tag_name = variables['OtherTags'][key]['Name']
            else:
                tag_name = key
            additional_tags[tag_name] = variables[key].ref

        volume = template.add_resource(
            ec2.Volume(
                'Volume',
                AvailabilityZone=variables['AvailabilityZone'].ref,
                Encrypted=If(
                    kmskeyidomitted,
                    False,
                    True),
                Iops=If(
                    io1voltype,
                    variables['Iops'].ref,
                    Ref('AWS::NoValue')),
                KmsKeyId=If(
                    kmskeyidomitted,
                    Ref('AWS::NoValue'),
                    variables['KmsKeyId'].ref),
                Size=If(
                    sizeomitted,
                    Ref('AWS::NoValue'),
                    variables['Size'].ref),
                SnapshotId=If(
                    snapshotidomitted,
                    Ref('AWS::NoValue'),
                    variables['SnapshotId'].ref),
                Tags=Tags(
                    application=variables['ApplicationName'].ref,
                    customer=variables['CustomerName'].ref,
                    environment=variables['EnvironmentName'].ref,
                    **additional_tags
                ),
                VolumeType=variables['VolumeType'].ref
            )
        )
        template.add_output(
            Output(
                'VolumeId',
                Description='Physical ID of volume',
                Value=Ref(volume)
            )
        )

        volumepolicy = template.add_resource(
            iam.ManagedPolicy(
                'VolumePolicy',
                Description=Join(
                    '',
                    ['Grants permission to attach volume ',
                     Ref(volume)]),
                Path='/',
                PolicyDocument=PolicyDocument(
                    Version='2012-10-17',
                    Statement=[
                        # Needed to attach volume with aws Chef cookbook
                        Statement(Action=[awacs.ec2.DescribeVolumes],
                                  Effect=Allow,
                                  Resource=['*']),
                        # Half of the necessary IAM permissions for an instance
                        # to attach the volume; combine with AttachVolume on
                        # the instance resource.
                        Statement(
                            Action=[awacs.ec2.AttachVolume],
                            Effect=Allow,
                            Resource=[
                                Join('', ['arn:aws:ec2:',
                                          Ref('AWS::Region'),
                                          ':',
                                          Ref('AWS::AccountId'),
                                          ':volume/',
                                          Ref(volume)])
                            ]
                        )
                    ]
                )
            )
        )
        template.add_output(
            Output(
                volumepolicy.title,
                Description='IAM managed policy granting permission to attach '
                            'the volume',
                Value=Ref(volumepolicy)
            )
        )


def version():
    """Call version function from top of repo."""
    root_dir = dirname(dirname(dirname(dirname(realpath(__file__)))))
    if root_dir not in sys.path:
        sys.path.append(root_dir)
    import platform_version  # pylint: disable=import-error
    return platform_version.version()


def updated_def_variables(variables, provided_var_dict, params_to_add):
    """Add CFN parameters to template based on the specified lists.

    Example params_to_add list:
        params_to_add = [
            {'var_name': 'OtherTags',
             'var_type': CFNString,
             'description': 'Extra tag value to apply to the instances'},
            {'var_name': 'OtherSGs',
             'var_type': CFNString,
             'description': 'Extra security group to apply to the instances'}
        ]
    """
    for param_to_add in params_to_add:
        if param_to_add['var_name'] in provided_var_dict:
            for key, _value in provided_var_dict[param_to_add['var_name']].value.items():  # noqa pylint: disable=C0301
                variables[key] = {
                    'type': param_to_add['var_type'],
                    'description': param_to_add['description']
                }
    return variables


def update_var_dict(provided_var_dict, params_to_add):
    """Return a dictionary to add to resolve_variables()'s variable_dict."""
    additional_vars = {}
    for param_to_add in params_to_add:
        if param_to_add['var_name'] in provided_var_dict:
            for key, value in provided_var_dict[param_to_add['var_name']].value.items():  # noqa pylint: disable=C0301
                if isinstance(value, (dict, OrderedDict)):
                    additional_vars[key] = Variable(key, dict(value)['Value'])
                else:
                    additional_vars[key] = Variable(key, value)
    return additional_vars


# Helper section to enable easy blueprint -> template generation
# (just run `python <thisfile>` to output the json)
if __name__ == "__main__":
    from stacker.context import Context

    print(EbsVol('test',
                 Context({'namespace': 'test'}),
                 None).to_json())
