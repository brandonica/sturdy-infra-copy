# ASG

This module provides semi-generic templates for server deployment.

## Standalone

This template was originally designed to support single-server ASGs, but can be used with multi-server ASGs as well.

## Replacing

This template sets up an ASG that is redeployed on every launch config update, and will roll back if its chef-client run doesn't complete successfully. It expects to be deployed with an AMI already including:

* chef-client
* awscli/AWS Tools for Windows Powershell # already included on Amazon Linux/Windows
* cfn-signal # already included on Amazon Linux/Windows

Tips:

* The chef-client run is required, but it can be as simple as basic service verification/tests.
