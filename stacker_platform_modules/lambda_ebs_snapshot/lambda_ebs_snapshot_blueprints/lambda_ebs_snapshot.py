#!/usr/bin/env python
"""Stacker module for setting up automatic EBS backup functions."""
from __future__ import print_function
from os import path
from os.path import dirname, realpath
import sys

from troposphere import (
    AWSHelperFn, GetAtt, Output, Ref, Sub, awslambda, events, iam
)

import awacs.ec2
import awacs.sts
from awacs.aws import Allow, Statement, PolicyDocument, Principal

from stacker.lookups.handlers.file import parameterized_codec
from stacker.blueprints.base import Blueprint
from stacker.blueprints.variables.types import CFNNumber, CFNString

AWS_LAMBDA_DIR = path.join(
    dirname(dirname(dirname(dirname(realpath(__file__))))),
    'src',
    'lambda_functions'
)
IAM_ARN_PREFIX = 'arn:aws:iam::aws:policy/service-role/'


class LambdaEbsSnapshot(Blueprint):
    """Extends Stacker Blueprint class."""

    snapshot_backup_src = parameterized_codec(
        open(path.join(AWS_LAMBDA_DIR, 'ebs_snapshot_backup.py'), 'r').read(),
        False  # disable base64 encoding
    )
    snapshot_cleanup_src = parameterized_codec(
        open(path.join(AWS_LAMBDA_DIR, 'ebs_snapshot_cleanup.py'), 'r').read(),
        False  # disable base64 encoding
    )

    VARIABLES = {
        # Can be overridden in the config:
        # ${file parameterized:file://lambda/snapshot_backup.py}
        'LambdaSnapshotBackup': {'type': AWSHelperFn,
                                 'description': 'Lambda function code',
                                 'default': snapshot_backup_src},
        # Can be overridden in the config:
        # ${file parameterized:file://lambda/snapshot_cleanup.py}
        'LambdaSnapshotCleanup': {'type': AWSHelperFn,
                                  'description': 'Lambda function code',
                                  'default': snapshot_cleanup_src},
        'BackupTagKey': {'type': CFNString,
                         'description': 'Ec2 tag key to look for while '
                                        'running backups',
                         'default': 'Backup'},
        'BackupTagValue': {'type': CFNString,
                           'description': 'Ec2 tag value to look for while '
                                          'running backups',
                           'default': 'Hourly'},
        'BackupRateValue': {'type': CFNNumber,
                            'description': 'A positive number for the backup '
                                           'rate - how often it runs',
                            'default': '1',
                            'min_value': '1'},
        'BackupRateUnit': {'type': CFNString,
                           'description': 'A unit of time for the backup rate',
                           'default': 'hour',
                           'allowed_values': ['minute',
                                              'minutes',
                                              'hour',
                                              'hours',
                                              'day',
                                              'days']},
        'BackupRetentionValue': {'type': CFNNumber,
                                 'description': 'A positive number of duration'
                                                'to keep snapshots, in days',
                                 'default': '7',
                                 'min_value': '1'},
        'BackupTimeout': {'type': CFNNumber,
                          'description': 'Maximum number of seconds backup '
                                         'function is allowed to run',
                          'default': '30'},
        'CleanupRateValue': {'type': CFNNumber,
                             'description': 'A positive number for the '
                                            'cleanup rate - how often it runs',
                             'default': '1',
                             'min_value': '1'},
        'CleanupRateUnit': {'type': CFNString,
                            'description': 'A unit of time for the backup '
                                           'rate',
                            'default': 'hour',
                            'allowed_values': ['minute',
                                               'minutes',
                                               'hour',
                                               'hours',
                                               'day',
                                               'days']},
        'CleanupTimeout': {'type': CFNNumber,
                           'description': 'Maximum number of seconds cleanup '
                                          'function is allowed to run',
                           'default': '30'}
    }

    def add_resources_and_outputs(self):
        """Add resources and outputs to template."""
        template = self.template
        variables = self.get_variables()

        lambdarole = template.add_resource(
            iam.Role(
                'LambdaRole',
                AssumeRolePolicyDocument=PolicyDocument(
                    Version='2012-10-17',
                    Statement=[
                        Statement(
                            Effect=Allow,
                            Action=[awacs.sts.AssumeRole],
                            Principal=Principal('Service',
                                                ['lambda.amazonaws.com'])
                        )
                    ]
                ),
                ManagedPolicyArns=[IAM_ARN_PREFIX +
                                   'AWSLambdaBasicExecutionRole'],
                Policies=[
                    iam.Policy(
                        PolicyName='lamba-ebs-snapshots-role',
                        PolicyDocument=PolicyDocument(
                            Version='2012-10-17',
                            Statement=[
                                Statement(
                                    Action=[
                                        awacs.aws.Action('ec2', 'Describe*'),
                                        awacs.ec2.CreateSnapshot,
                                        awacs.ec2.DeleteSnapshot,
                                        awacs.ec2.CreateTags,
                                        awacs.ec2.ModifySnapshotAttribute,
                                        awacs.ec2.ResetSnapshotAttribute],
                                    Effect=Allow,
                                    Resource=['*']
                                )
                            ]
                        )
                    )
                ]
            )
        )

        # If uploaded to S3 via stacker hook, use that URL; otherwise fall back
        # to the inline code
        if ('lambda' in self.context.hook_data and
                'LambdaSnapshotBackup' in self.context.hook_data['lambda']):
            backup_code = (
                self.context.hook_data['lambda']['LambdaSnapshotBackup']
            )
            backup_handler = 'ebs_snapshot_backup.lambda_handler'
        else:
            backup_code = awslambda.Code(
                ZipFile=variables['LambdaSnapshotBackup']
            )
            backup_handler = 'index.lambda_handler'

        lambdasnapshotbackup = template.add_resource(
            awslambda.Function(
                'LambdaSnapshotBackup',
                Description=Sub('Takes Snapshots of servers with Tag '
                                '${BackupTagKey}:${BackupTagValue}'),
                Code=backup_code,
                Environment=awslambda.Environment(
                    Variables={
                        'TagKey': Sub('${BackupTagKey}'),
                        'TagValue': Sub('${BackupTagValue}'),
                        'BackupRetention': Sub('${BackupRetentionValue}')
                    }
                ),
                Handler=backup_handler,
                Role=GetAtt(lambdarole, 'Arn'),
                Runtime='python2.7',
                Timeout=variables['BackupTimeout'].ref
            )
        )
        template.add_output(Output(
            '%sName' % lambdasnapshotbackup.title,
            Description='Name of lambda function performing EBS backups',
            Value=Ref(lambdasnapshotbackup)
        ))

        lambdabackupevent = template.add_resource(
            events.Rule(
                'LambdaBackupEvent',
                Description='An event to schedule snapshot backups',
                ScheduleExpression=Sub('rate(${BackupRateValue} '
                                       '${BackupRateUnit})'),
                State='ENABLED',
                Targets=[events.Target(
                    Arn=GetAtt(lambdasnapshotbackup, 'Arn'),
                    Id='TargetFunctionV1'
                )]
            )
        )

        template.add_resource(
            awslambda.Permission(
                'LambdaBackupPermission',
                Action='lambda:InvokeFunction',
                FunctionName=GetAtt(lambdasnapshotbackup, 'Arn'),
                Principal='events.amazonaws.com',
                SourceArn=GetAtt(lambdabackupevent, 'Arn')
            )
        )

        # If uploaded to S3 via stacker hook, use that URL; otherwise fall back
        # to the inline code
        if ('lambda' in self.context.hook_data and
                'LambdaSnapshotCleanup' in self.context.hook_data['lambda']):
            cleanup_code = (
                self.context.hook_data['lambda']['LambdaSnapshotCleanup']
            )
            cleanup_handler = 'ebs_snapshot_cleanup.lambda_handler'
        else:
            cleanup_code = awslambda.Code(
                ZipFile=variables['LambdaSnapshotCleanup']
            )
            cleanup_handler = 'index.lambda_handler'

        lambdasnapshotcleanup = template.add_resource(
            awslambda.Function(
                'LambdaSnapshotCleanup',
                Description='Cleans up the old snapshots that contain Key '
                            'DeleteOn',
                Code=cleanup_code,
                Environment=awslambda.Environment(
                    Variables={
                        'AccountId': Ref('AWS::AccountId')
                    }
                ),
                Handler=cleanup_handler,
                Role=GetAtt(lambdarole, 'Arn'),
                Runtime='python2.7',
                Timeout=variables['CleanupTimeout'].ref
            )
        )
        template.add_output(Output(
            '%sName' % lambdasnapshotcleanup.title,
            Description='Name of lambda function performing EBS snapshot '
                        'cleanup',
            Value=Ref(lambdasnapshotcleanup)
        ))

        lambdacleanupevent = template.add_resource(
            events.Rule(
                'LambdaCleanupEvent',
                Description='An Event for Snapshot cleanups',
                ScheduleExpression=Sub('rate(${CleanupRateValue} '
                                       '${CleanupRateUnit})'),
                State='ENABLED',
                Targets=[events.Target(
                    Arn=GetAtt(lambdasnapshotcleanup, 'Arn'),
                    Id='TargetFunctionV1'
                )]
            )
        )

        template.add_resource(
            awslambda.Permission(
                'LambdaCleanupPermission',
                Action='lambda:InvokeFunction',
                FunctionName=GetAtt(lambdasnapshotcleanup, 'Arn'),
                Principal='events.amazonaws.com',
                SourceArn=GetAtt(lambdacleanupevent, 'Arn')
            )
        )

    def create_template(self):
        """Create template (main function called by Stacker)."""
        self.template.add_version('2010-09-09')
        self.template.add_description("Sturdy Platform - Lambda EBS Snapshots "
                                      "- {0}".format(version()))
        self.add_resources_and_outputs()


def version():
    """Call version function from top of repo."""
    root_dir = dirname(dirname(dirname(dirname(realpath(__file__)))))
    if root_dir not in sys.path:
        sys.path.append(root_dir)
    import platform_version  # pylint: disable=import-error
    return platform_version.version()


# Helper section to enable easy blueprint -> template generation
# (just run `python <thisfile>` to output the json)
if __name__ == "__main__":
    from stacker.context import Context

    print(LambdaEbsSnapshot('test',
                            Context({'namespace': 'test'}),
                            None).to_json())
