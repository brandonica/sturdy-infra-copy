# Automatic EBS Snapshots

## Description

### Backup Function

This function evaluates all instances using a filter of the `BackupTagKey` EC2 tag and will start an EBS snapshot of all volumes if the `BackupTagValue` value matches. The function will retain the backup for the number of days set as a positive integer in the `Retention` tag.  If no `Retention` tag is set, the default retention value set as the `BackupRetentionValue` variable will be used instead. The snapshot will be tagged with the `DeleteOn` tag containing a date that the cleanup lambda function looks for.

### Cleanup Function

This function looks at all snapshots and deletes ones marked with the target `DeleteOn` key containing a value after the date/time set. Backups that do not contain the `DeleteOn` will not be touched.

### Retention

Backup retention is based on EC2 tags with a key of `Retention` and a positive number as the value to represent the number of days to keep the backup. If you do not set this tag on an instance that happens to have the the `BackupTagKey` tag with matching `BackupTagValue` value, the value set under "BackupRetentionValue" will be used.  The Stacker blueprint and Terraform module have a default value of 7 days if none is set in your config.

## Deployment

For standard deployments, deploy via the default configuration yaml (updating the repository tag as needed):
```
namespace: ${namespace}
stacker_bucket: ${stacker_bucket_name}

package_sources:
  git:
    - uri: git@bitbucket.org:nbdev/sturdy-platform-infrastructure.git
      tag: v1.1.0
      paths:
        - stacker_platform_modules/core
        - stacker_platform_modules/lambda_ebs_snapshot
      configs:
        - stacker_platform_modules/lambda_ebs_snapshot/lambda_ebs_snapshot.yaml
```
