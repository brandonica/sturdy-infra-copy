"""Wrapper for aws_lambda hook adding support for functions in remote pkgs."""
from os import path

import stacker.hooks.aws_lambda as aws_lambda


def prepend_src_dir(name):
    """Update directory to an absolute reference to the repo 'src' dir."""
    if path.isabs(name):
        return name
    else:
        # Updating
        repo_dir = (
            path.dirname(
                # stacker_platform_modules
                path.dirname(
                    # lambda_ebs_snapshot
                    path.dirname(
                        # lambda_ebs_snapshot_hooks
                        path.dirname(
                            path.realpath(__file__)
                        )
                    )
                )
            )
        )
        return path.join(repo_dir, 'src', name)


def upload_remote_lambda_functions(context, provider, **kwargs):
    """Wrap upload_lambda_functions and fix cached paths."""
    functions = {}
    for name, options in kwargs['functions'].items():
        functions[name] = options
        if not path.exists(options['path']):
            functions[name]['path'] = prepend_src_dir(options['path'])
    return aws_lambda.upload_lambda_functions(context=context,
                                              provider=provider,
                                              functions=functions)
