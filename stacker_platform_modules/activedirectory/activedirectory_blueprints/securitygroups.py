#!/usr/bin/env python
"""Module with AD security groups."""
from __future__ import print_function
from os.path import dirname, realpath
import sys

from troposphere import Join, Output, Ref, Tags, ec2

from stacker.blueprints.base import Blueprint
from stacker.blueprints.variables.types import CFNString, EC2VPCId


class SecurityGroups(Blueprint):
    """Stacker blueprint for AD security groups."""

    VARIABLES = {
        'CustomerName': {'type': CFNString,
                         'description': 'The nickname for the customer/tenant.'
                                        ' Must be all lowercase letters, '
                                        'should not contain spaces or special '
                                        'characters, nor should it include '
                                        'any part of EnvironmentName.',
                         'allowed_pattern': '[-_ a-z]*',
                         'default': ''},
        'EnvironmentName': {'type': CFNString,
                            'description': 'Name of Environment',
                            'default': 'common'},
        'ReplicationCidrBlocks': {'type': dict,
                                  'description': 'IP ranges to which AD '
                                                 'replication access will be '
                                                 'granted.',
                                  'default': {}},
        'CidrBlocks': {'type': dict,
                       'description': 'IP ranges to which access will be '
                                      'granted',
                       'default': {'Main': '0.0.0.0/0'}},
        'VpcId': {'type': EC2VPCId,
                  'description': 'VPC id.'}
    }

    def create_template(self):
        """Create template (main function called by Stacker)."""
        template = self.template
        variables = self.get_variables()
        template.add_version('2010-09-09')
        template.add_description("Sturdy Platform - Active Directory - "
                                 "Security Groups - "
                                 "{0}".format(version()))

        # https://wiki.samba.org/index.php/Samba_AD_DC_Port_Usage
        # https://technet.microsoft.com/en-us/library/dd772723%28v=ws.10%29.aspx?f=255&MSPPError=-2147217396
        # https://support.microsoft.com/en-gb/help/179442/how-to-configure-a-firewall-for-domains-and-trusts
        ingress_rules = []
        for cidr in sorted(variables['CidrBlocks']):
            for i in ['tcp', 'udp']:
                # DNS
                ingress_rules.append(ec2.SecurityGroupRule(
                    IpProtocol=i,
                    FromPort='53',
                    ToPort='53',
                    CidrIp=variables['CidrBlocks'][cidr]
                ))
                # Kerberos
                ingress_rules.append(ec2.SecurityGroupRule(
                    IpProtocol=i,
                    FromPort='88',
                    ToPort='88',
                    CidrIp=variables['CidrBlocks'][cidr]
                ))
                # LDAP
                ingress_rules.append(ec2.SecurityGroupRule(
                    IpProtocol=i,
                    FromPort='389',
                    ToPort='389',
                    CidrIp=variables['CidrBlocks'][cidr]
                ))
                # SMB, CIFS, SMB2, DFSN, LSARPC, NbtSS, NetLogonR, SamR, SrvSvc
                ingress_rules.append(ec2.SecurityGroupRule(
                    IpProtocol=i,
                    FromPort='445',
                    ToPort='445',
                    CidrIp=variables['CidrBlocks'][cidr]
                ))
                # Kerberos kpasswd
                ingress_rules.append(ec2.SecurityGroupRule(
                    IpProtocol=i,
                    FromPort='464',
                    ToPort='464',
                    CidrIp=variables['CidrBlocks'][cidr]
                ))
                # Dynamic RPC Ports
                # For Samba <4.7
                ingress_rules.append(ec2.SecurityGroupRule(
                    IpProtocol=i,
                    FromPort='1024',
                    ToPort='1300',
                    CidrIp=variables['CidrBlocks'][cidr]
                ))
                # For Samba 4.7+
                ingress_rules.append(ec2.SecurityGroupRule(
                    IpProtocol=i,
                    FromPort='49152',
                    ToPort='65535',
                    CidrIp=variables['CidrBlocks'][cidr]
                ))

            for i in [['icmp', '-1'],  # ICMP
                      ['tcp', '25'],  # SMTP
                      ['udp', '123'],  # Windows Time
                      ['tcp', '135'],  # End point mapper
                      ['udp', '137'],  # NetBIOS Name Service
                      ['udp', '138'],  # NetBIOS Datagram
                      ['tcp', '139'],  # NetBIOS Session
                      ['tcp', '636'],  # LDAPS
                      ['tcp', '3268'],  # Global Catalog
                      ['tcp', '3269'],  # Global Catalog SSL
                      ['tcp', '5722'],  # RPC, DFSR (SYSVOL)
                      ['tcp', '9389']]:  # SOAP
                ingress_rules.append(ec2.SecurityGroupRule(
                    IpProtocol=i[0],
                    FromPort=i[1],
                    ToPort=i[1],
                    CidrIp=variables['CidrBlocks'][cidr]
                ))

            # Keycloak
            ingress_rules.append(ec2.SecurityGroupRule(
                IpProtocol='tcp',
                FromPort='8443',
                ToPort='8443',
                CidrIp=variables['CidrBlocks'][cidr]
            ))

        # https://docs.aws.amazon.com/directoryservice/latest/admin-guide/tutorial_setup_trust_prepare_onprem.html#tutorial_setup_trust_connect_vpc
        for cidr in sorted(variables['ReplicationCidrBlocks']):
            if (variables['ReplicationCidrBlocks'][cidr] is None or
                    variables['ReplicationCidrBlocks'][cidr] == 'none'):
                continue
            for i in ['tcp', 'udp']:
                # DNS
                ingress_rules.append(ec2.SecurityGroupRule(
                    IpProtocol=i,
                    FromPort='53',
                    ToPort='53',
                    CidrIp=variables['ReplicationCidrBlocks'][cidr]
                ))
                # Kerberos
                ingress_rules.append(ec2.SecurityGroupRule(
                    IpProtocol=i,
                    FromPort='88',
                    ToPort='88',
                    CidrIp=variables['ReplicationCidrBlocks'][cidr]
                ))
                # LDAP
                ingress_rules.append(ec2.SecurityGroupRule(
                    IpProtocol=i,
                    FromPort='389',
                    ToPort='389',
                    CidrIp=variables['ReplicationCidrBlocks'][cidr]
                ))
            # LDAPS
            ingress_rules.append(ec2.SecurityGroupRule(
                IpProtocol='tcp',
                FromPort='636',
                ToPort='636',
                CidrIp=variables['ReplicationCidrBlocks'][cidr]
            ))
            # SMB
            ingress_rules.append(ec2.SecurityGroupRule(
                IpProtocol='tcp',
                FromPort='445',
                ToPort='445',
                CidrIp=variables['ReplicationCidrBlocks'][cidr]
            ))

        serversecuritygroup = template.add_resource(
            ec2.SecurityGroup(
                'ServerSecurityGroup',
                GroupDescription=Join('-', [variables['CustomerName'].ref,
                                            'ad-server']),
                SecurityGroupIngress=ingress_rules,
                SecurityGroupEgress=[
                    ec2.SecurityGroupRule(
                        IpProtocol='-1',
                        FromPort='0',
                        ToPort='65535',
                        CidrIp='0.0.0.0/0')
                ],
                Tags=Tags(
                    Name=Join(
                        '-',
                        [variables['CustomerName'].ref,
                         'ad-server',
                         variables['EnvironmentName'].ref]
                    )
                ),
                VpcId=Ref('VpcId')
            )
        )
        template.add_output(
            Output(
                serversecuritygroup.title,
                Description='Security group ID',
                Value=Ref(serversecuritygroup)
            )
        )

        # https://wiki.samba.org/index.php/Rsync_based_SysVol_replication_workaround
        template.add_resource(
            ec2.SecurityGroupIngress(
                'SysvolReplication',
                FromPort='873',
                GroupId=Ref(serversecuritygroup),
                IpProtocol='tcp',
                SourceSecurityGroupId=Ref(serversecuritygroup),
                ToPort='873'
            )
        )

        loadbalancersecuritygroup = template.add_resource(
            ec2.SecurityGroup(
                'LoadBalancerSecurityGroup',
                GroupDescription=Join('-', [variables['CustomerName'].ref,
                                            'ad-lb']),
                SecurityGroupIngress=[
                    ec2.SecurityGroupRule(
                        IpProtocol='tcp',
                        FromPort='8443',
                        ToPort='8443',
                        CidrIp='0.0.0.0/0')
                ],
                SecurityGroupEgress=[
                    ec2.SecurityGroupRule(
                        IpProtocol='-1',
                        FromPort='0',
                        ToPort='65535',
                        CidrIp='0.0.0.0/0')
                ],
                Tags=Tags(
                    Name=Join(
                        '-',
                        [variables['CustomerName'].ref,
                         'ad-lb',
                         variables['EnvironmentName'].ref]
                    )
                ),
                VpcId=Ref('VpcId')
            )
        )
        template.add_output(
            Output(
                loadbalancersecuritygroup.title,
                Description='Security group ID',
                Value=Ref(loadbalancersecuritygroup)
            )
        )


def version():
    """Call version function from top of repo."""
    root_dir = dirname(dirname(dirname(dirname(realpath(__file__)))))
    if root_dir not in sys.path:
        sys.path.append(root_dir)
    import platform_version  # pylint: disable=import-error
    return platform_version.version()


# Helper section to enable easy blueprint -> template generation
# (just run `python <thisfile>` to output the json)
if __name__ == "__main__":
    from stacker.context import Context

    print(SecurityGroups('test',
                         Context({'namespace': 'test'}),
                         None).to_json())
