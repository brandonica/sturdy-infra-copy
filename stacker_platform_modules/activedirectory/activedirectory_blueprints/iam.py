#!/usr/bin/env python
"""Module with AD IAM roles/policies."""
from __future__ import print_function
from os.path import dirname, realpath
import sys

from troposphere import And, Equals, If, Join, Not, Output, Ref, iam

import awacs.ec2
import awacs.route53
import awacs.sts
from awacs.aws import Allow, PolicyDocument, Statement

from stacker.blueprints.base import Blueprint
from stacker.blueprints.variables.types import CFNString


class Iam(Blueprint):
    """Stacker blueprint for AD roles/policies."""

    VARIABLES = {
        'HostedZoneID': {'type': CFNString,
                         'description': 'Route53 hosted zone ID.',
                         'default': ''}
    }

    def create_template(self):
        """Create template (main function called by Stacker)."""
        template = self.template
        variables = self.get_variables()
        template.add_version('2010-09-09')
        template.add_description("Sturdy Platform - Active Directory - IAM "
                                 "- {0}".format(version()))

        # Conditions
        hostedzoneidspecified = 'HostedZoneIDSpecified'
        template.add_condition(
            hostedzoneidspecified,
            And(Not(Equals(variables['HostedZoneID'].ref, '')),
                Not(Equals(variables['HostedZoneID'].ref, 'undefined')))
        )

        # Resources
        serverpolicy = template.add_resource(
            iam.ManagedPolicy(
                'ServerPolicy',
                Description='Active Directory managed policy.',
                Path='/',
                PolicyDocument=If(
                    hostedzoneidspecified,
                    PolicyDocument(
                        Version='2012-10-17',
                        Statement=[
                            # For optional ElasticIP association
                            Statement(
                                Action=[awacs.ec2.DescribeAddresses,
                                        awacs.ec2.AssociateAddress],
                                Effect=Allow,
                                Resource=['*']
                            ),
                            # https://github.com/certbot/certbot/blob/v0.17.0/certbot-dns-route53/examples/sample-aws-policy.json
                            Statement(
                                Action=[awacs.route53.ListHostedZones,
                                        awacs.route53.GetChange],
                                Effect=Allow,
                                Resource=['*']
                            ),
                            Statement(
                                Action=[
                                    awacs.route53.ChangeResourceRecordSets
                                ],
                                Effect=Allow,
                                Resource=[
                                    Join('',
                                         ['arn:aws:route53:::hostedzone/',
                                          variables['HostedZoneID'].ref])]
                            )
                        ]
                    ),
                    PolicyDocument(
                        Version='2012-10-17',
                        Statement=[]
                    )
                )
            )
        )
        template.add_output(
            Output(
                serverpolicy.title,
                Description='AD server policy',
                Value=Ref(serverpolicy)
            )
        )


def version():
    """Call version function from top of repo."""
    root_dir = dirname(dirname(dirname(dirname(realpath(__file__)))))
    if root_dir not in sys.path:
        sys.path.append(root_dir)
    import platform_version  # pylint: disable=import-error
    return platform_version.version()


# Helper section to enable easy blueprint -> template generation
# (just run `python <thisfile>` to output the json)
if __name__ == "__main__":
    from stacker.context import Context

    print(Iam('test',
              Context({'namespace': 'test'}),
              None).to_json())
