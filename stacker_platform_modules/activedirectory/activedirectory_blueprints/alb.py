#!/usr/bin/env python
"""Module with AD load balancer."""
from __future__ import print_function
from os.path import dirname, realpath
import sys

from troposphere import (
    And, Equals, GetAtt, Join, Not, Output, Ref, Tags, elasticloadbalancingv2,
    route53
)

from stacker.blueprints.base import Blueprint
from stacker.blueprints.variables.types import (
    CFNString, EC2SecurityGroupIdList, EC2SubnetIdList, EC2VPCId
)


class LoadBalancer(Blueprint):
    """Stacker blueprint for AD load balancer."""

    VARIABLES = {
        'CustomerName': {'type': CFNString,
                         'description': 'The nickname for the customer/tenant.'
                                        ' Must be all lowercase letters, '
                                        'should not contain spaces or special '
                                        'characters, nor should it include '
                                        'any part of EnvironmentName.',
                         'allowed_pattern': '[-_ a-z]*',
                         'default': ''},
        'EnvironmentName': {'type': CFNString,
                            'description': 'Name of Environment',
                            'default': 'common'},
        'CertificateArn': {'type': CFNString,
                           'description': 'TLS cert ARN'},
        'LoadBalancerName': {'type': CFNString,
                             'description': 'Name for load balancer',
                             'default': 'ad-lb'},
        'LoadBalancerScheme': {'type': CFNString,
                               'description': 'Load balancer scheme',
                               'default': 'internet-facing'},
        'SecurityGroups': {'type': EC2SecurityGroupIdList,
                           'description': 'Security groups to apply to the '
                                          'load balancer'},
        'Subnets': {'type': EC2SubnetIdList,
                    'description': 'Subnets in which the load balancer '
                                   'will be deployed'},
        'VpcId': {'type': EC2VPCId,
                  'description': 'VPC id.'},
        'HostedZoneID': {'type': CFNString,
                         'description': 'Route53 hosted zone ID.',
                         'default': ''},
        'Route53RecordName': {'type': CFNString,
                              'description': 'Name of DNS entry.',
                              'default': ''},
        'Route53TTL': {'type': CFNString,
                       'description': 'Record TTL.',
                       'default': '600'},
        'Route53Type': {'type': CFNString,
                        'description': 'Type of record.',
                        'default': 'CNAME'}

    }

    def create_template(self):
        """Create template (main function called by Stacker)."""
        template = self.template
        variables = self.get_variables()
        template.add_version('2010-09-09')
        template.add_description("Sturdy Platform - Active Directory - "
                                 "Load Balancer - "
                                 "{0}".format(version()))

        # Conditions
        hostedzoneidspecified = 'HostedZoneIDSpecified'
        template.add_condition(
            hostedzoneidspecified,
            And(Not(Equals(variables['HostedZoneID'].ref, '')),
                Not(Equals(variables['HostedZoneID'].ref, 'undefined')))
        )

        # Resources
        loadbalancer = template.add_resource(
            elasticloadbalancingv2.LoadBalancer(
                'LoadBalancer',
                Name=Join(
                    '-',
                    [variables['CustomerName'].ref,
                     variables['LoadBalancerName'].ref,
                     variables['EnvironmentName'].ref]
                ),
                SecurityGroups=variables['SecurityGroups'].ref,
                Scheme=variables['LoadBalancerScheme'].ref,
                Subnets=variables['Subnets'].ref,
                Tags=Tags(
                    CustomerName=variables['CustomerName'].ref,
                    Environment=variables['EnvironmentName'].ref
                )
            )
        )
        template.add_output(
            Output(
                loadbalancer.title,
                Description='Load balancer ARN',
                Value=Ref(loadbalancer)
            )
        )
        template.add_output(
            Output(
                "%sDNSName" % loadbalancer.title,
                Description='Load balancer DNS name',
                Value=GetAtt(loadbalancer, 'DNSName')
            )
        )

        targetgroup = template.add_resource(
            elasticloadbalancingv2.TargetGroup(
                'TargetGroup',
                HealthCheckIntervalSeconds='60',
                HealthCheckPort='8443',
                HealthCheckProtocol='HTTPS',
                HealthCheckTimeoutSeconds='10',
                HealthyThresholdCount='2',
                Matcher=elasticloadbalancingv2.Matcher(
                    HttpCode='200'),
                Port='8443',
                Protocol='HTTPS',
                UnhealthyThresholdCount='3',
                VpcId=variables['VpcId'].ref
            )
        )
        template.add_output(
            Output(
                targetgroup.title,
                Description='Target group ARN',
                Value=Ref(targetgroup)
            )
        )

        template.add_resource(
            elasticloadbalancingv2.Listener(
                'Listener',
                Certificates=[
                    elasticloadbalancingv2.Certificate(
                        CertificateArn=variables['CertificateArn'].ref
                    )
                ],
                Port='8443',
                Protocol='HTTPS',
                LoadBalancerArn=Ref(loadbalancer),
                DefaultActions=[elasticloadbalancingv2.Action(
                    Type='forward',
                    TargetGroupArn=Ref(targetgroup)
                )],
                SslPolicy='ELBSecurityPolicy-2016-08'  # TLS v1.0
            )
        )

        template.add_resource(
            route53.RecordSetType(
                'DNSEntry',
                Condition=hostedzoneidspecified,
                HostedZoneId=variables['HostedZoneID'].ref,
                Name=variables['Route53RecordName'].ref,
                ResourceRecords=[GetAtt(loadbalancer, 'DNSName')],
                TTL=variables['Route53TTL'].ref,
                Type=variables['Route53Type'].ref
            )
        )


def version():
    """Call version function from top of repo."""
    root_dir = dirname(dirname(dirname(dirname(realpath(__file__)))))
    if root_dir not in sys.path:
        sys.path.append(root_dir)
    import platform_version  # pylint: disable=import-error
    return platform_version.version()


# Helper section to enable easy blueprint -> template generation
# (just run `python <thisfile>` to output the json)
if __name__ == "__main__":
    from stacker.context import Context

    print(LoadBalancer('test',
                       Context({'namespace': 'test'}),
                       None).to_json())
