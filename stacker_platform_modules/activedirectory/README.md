# Active Directory

This module supports deploying automated Active Directory systems.

## Prerequisites

* Route53 hosted zone for the directory's parent domain
* ACM certificate for the directory domain name
* Admin password stored in SSM Parameter Store as `${environment}.activedirectory.admin_pass`
    * This can and should be deleted after initial provisioning and user accounts are created.
* Defined stacker environment variables:
```
activedirectory_domain: XXXX # e.g. directory.foo.com
activedirectory_netbios: XXXX # e.g. FOOCOM
activedirectory_upn_suffix: XXXX # e.g. foo.com
activedirectory_route53_hostedzone: XXXX e.g. Z1KU1234567
activedirectory_route53_recordname_1: XXXX # e.g. ad1.directory.foo.com
activedirectory_lb_cert_arn: XXXX # i.e. ACM certificate for the directory domain name
```

## Single Server

Consume in your environment with a config like the following:
```
namespace: ${namespace}
stacker_bucket: ${stacker_bucket_name}

package_sources:
  git:
    - uri: git@bitbucket.org:nbdev/sturdy-platform-infrastructure.git
      tag: vX.X.X
      paths:
        - stacker_platform_modules/activedirectory
        - stacker_platform_modules/asg
        - stacker_platform_modules/core
      configs:
        - stacker_platform_modules/activedirectory/loadbalancer-2-subnets.yaml
        - stacker_platform_modules/activedirectory/ad1-standalone.yaml
```

## Multiple Servers

Add Stacker environment file entries in the following form for each additional
domain controller:
```
activedirectory_route53_recordname_X: XXXX # e.g. adX.directory.foo.com
```

Run a separate `bootstrap-X-dc.yaml` config file (where X is the number of domain
controllers) prior to the domain controller config files, e.g.:
```
namespace: ${namespace}
stacker_bucket: ${stacker_bucket_name}

package_sources:
  git:
    - uri: git@bitbucket.org:nbdev/sturdy-platform-infrastructure.git
      tag: vX.X.X
      paths:
        - stacker_platform_modules/activedirectory
        - stacker_platform_modules/asg
        - stacker_platform_modules/core
      configs:
        - stacker_platform_modules/activedirectory/bootstrap-2-dc.yaml
```

In the primary config file, add `adX.yaml` config files for each additional
domain controller, e.g.:
```
namespace: ${namespace}
stacker_bucket: ${stacker_bucket_name}

package_sources:
  git:
    - uri: git@bitbucket.org:nbdev/sturdy-platform-infrastructure.git
      tag: vX.X.X
      paths:
        - stacker_platform_modules/activedirectory
        - stacker_platform_modules/asg
        - stacker_platform_modules/core
      configs:
        - stacker_platform_modules/activedirectory/loadbalancer-2-subnets.yaml
        - stacker_platform_modules/activedirectory/ad1.yaml
        - stacker_platform_modules/activedirectory/ad2.yaml
```
or
```
namespace: ${namespace}
stacker_bucket: ${stacker_bucket_name}

package_sources:
  git:
    - uri: git@bitbucket.org:nbdev/sturdy-platform-infrastructure.git
      tag: vX.X.X
      paths:
        - stacker_platform_modules/activedirectory
        - stacker_platform_modules/asg
        - stacker_platform_modules/core
      configs:
        - stacker_platform_modules/activedirectory/loadbalancer-3-subnets.yaml
        - stacker_platform_modules/activedirectory/ad1.yaml
        - stacker_platform_modules/activedirectory/ad2.yaml
        - stacker_platform_modules/activedirectory/ad3.yaml
```
