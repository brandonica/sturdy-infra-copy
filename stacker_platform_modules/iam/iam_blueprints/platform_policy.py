#!/usr/bin/env python
"""Module for platform modules IAM policy."""
from __future__ import print_function
from os.path import dirname, realpath
import sys

from collections import OrderedDict

from troposphere import Join, Output, Ref, iam

import awacs.autoscaling
import awacs.awslambda
import awacs.cloudformation
import awacs.cloudwatch
import awacs.ec2
import awacs.iam
import awacs.logs
import awacs.s3
import awacs.sns
import awacs.ssm
from awacs.aws import Allow, PolicyDocument, Statement

from stacker.blueprints.base import Blueprint
from stacker.blueprints.variables.types import CFNString


CFN_ACTIONS = [awacs.cloudformation.CreateStack,
               awacs.cloudformation.DeleteStack,
               awacs.cloudformation.CreateChangeSet,
               awacs.cloudformation.DescribeChangeSet,
               awacs.cloudformation.DeleteChangeSet,
               awacs.cloudformation.DescribeStacks,
               awacs.cloudformation.DescribeStackEvents,
               awacs.cloudformation.DescribeStackResource,  # Serverless
               awacs.cloudformation.DescribeStackResources,  # Serverless
               awacs.cloudformation.GetTemplate,  # Stacker diff
               awacs.cloudformation.UpdateStack,
               awacs.cloudformation.ExecuteChangeSet,
               awacs.cloudformation.ValidateTemplate]  # Serverless


def get_str_class():
    """Return string class for the running version of python."""
    if sys.version_info[0] == 2:
        return basestring
    return str


class PlatformPolicy(Blueprint):
    """Stacker blueprint for platform modules IAM policy."""

    VARIABLES = {
        'IamPermissions': {'type': get_str_class(),
                           'description': 'Comma-separated list of IAM '
                                          'permissions for the managed '
                                          'policy.',
                           'default': ''},
        'ConfigBucketArn': {'type': CFNString,
                            'description': '(Optional) Bucket containting '
                                           'Chef configs; will be used if the '
                                           '`chef_configs` IamPermission is '
                                           'granted.',
                            'default': ''},
        'EnvironmentName': {'type': CFNString,
                            'description': '(Optional) Name of Environment; '
                                           'will be used if the `ssm` '
                                           'IamPermission is granted.',
                            'default': 'common'},
        'BucketKey': {'type': CFNString,
                      'description': '(Optional) S3 key prefix for use with '
                                     'the `ssm` IamPermission is granted',
                      'default': ''}
    }

    def create_template(self):  # pylint: disable=too-many-branches
        """Create template (main function called by Stacker)."""
        template = self.template
        variables = self.get_variables()
        template.add_version('2010-09-09')
        template.add_description("Sturdy Platform - IAM - Platform Management "
                                 "Policy - {0}".format(version()))

        # Resources

        # Dynamically build the CFN template with the specified permissions
        # (see README for `jenkins_iam_permissions`)
        iam_statements = [
            Statement(  # Platform downloading
                Action=[awacs.s3.ListBucket,
                        awacs.s3.GetObject],
                Effect=Allow,
                Resource=[
                    'arn:aws:s3:::sturdy-platform*',
                    'arn:aws:s3:::onica-platform*',
                ]
            )
        ]
        if variables['IamPermissions'] not in ['', 'undefined']:
            requested_permissions = variables['IamPermissions'].split(',')
            if 'core' in requested_permissions:
                for i in ['asg', 'stacker_bucket']:
                    if i not in requested_permissions:
                        requested_permissions.append(i)
            actions_to_dedupe = []
            if 'stacker_bucket' in requested_permissions:
                iam_statements.extend([
                    Statement(  # Stacker bucket management
                        Action=[
                            awacs.s3.CreateBucket,
                            awacs.aws.Action('s3', 'HeadBucket'),
                            awacs.s3.ListBucket
                        ],
                        Effect=Allow,
                        Resource=['arn:aws:s3:::stacker-*']
                    ),
                    Statement(  # Stacker template management
                        Action=[
                            awacs.s3.GetObject,
                            awacs.s3.PutObject,
                            awacs.s3.PutObjectAcl
                        ],
                        Effect=Allow,
                        Resource=['arn:aws:s3:::stacker-*/*']
                    )
                ])
            if 'chef_configs' in requested_permissions:
                iam_statements.append(
                    Statement(
                        Action=[
                            awacs.s3.PutObject
                        ],
                        Effect=Allow,
                        Resource=[
                            Join('', [variables['ConfigBucketArn'].ref,
                                      '/*'])
                        ]
                    )
                )
            if 'ssm' in requested_permissions and (
                    variables['BucketKey'].value != ''):
                iam_statements.append(
                    Statement(
                        Action=[
                            awacs.ssm.DeleteParameter,
                            awacs.aws.Action('ssm', 'GetParameter'),
                            awacs.ssm.GetParameters,
                            awacs.ssm.GetParameterHistory,
                            awacs.ssm.PutParameter
                        ],
                        Effect=Allow,
                        Resource=[
                            Join('', ['arn:aws:ssm:',
                                      Ref('AWS::Region'),
                                      ':',
                                      Ref('AWS::AccountId'),
                                      ':parameter/',
                                      variables['EnvironmentName'].ref,
                                      '.',
                                      variables['BucketKey'].ref,
                                      '.*']),
                            Join('', ['arn:aws:ssm:',
                                      Ref('AWS::Region'),
                                      ':',
                                      Ref('AWS::AccountId'),
                                      ':parameter/',
                                      variables['EnvironmentName'].ref,
                                      '/',
                                      variables['BucketKey'].ref,
                                      '/*'])
                        ]
                    )
                )
            if 'cloudformation' in requested_permissions:
                actions_to_dedupe.extend(CFN_ACTIONS)
            if 'packer' in requested_permissions:
                # https://www.packer.io/docs/builders/amazon.html#using-an-iam-task-or-instance-role
                actions_to_dedupe.extend([
                    awacs.ec2.AttachVolume,
                    awacs.ec2.AuthorizeSecurityGroupIngress,
                    awacs.ec2.CopyImage,
                    awacs.ec2.CreateImage,
                    awacs.ec2.CreateKeyPair,
                    awacs.ec2.CreateSecurityGroup,
                    awacs.ec2.CreateSnapshot,
                    awacs.ec2.CreateTags,
                    awacs.ec2.DeleteKeyPair,
                    awacs.ec2.CreateVolume,
                    awacs.ec2.DeleteSecurityGroup,
                    awacs.ec2.DeleteSnapshot,
                    awacs.ec2.DeleteVolume,
                    awacs.ec2.DeregisterImage,
                    awacs.ec2.DescribeImageAttribute,
                    awacs.ec2.DescribeImages,
                    awacs.ec2.DescribeInstances,
                    awacs.ec2.DescribeRegions,
                    awacs.ec2.DescribeSecurityGroups,
                    awacs.ec2.DescribeSnapshots,
                    awacs.ec2.DescribeSubnets,
                    awacs.ec2.DescribeTags,
                    awacs.ec2.DescribeVolumes,
                    awacs.ec2.DetachVolume,
                    awacs.ec2.GetPasswordData,
                    awacs.ec2.ModifyImageAttribute,
                    awacs.ec2.ModifyInstanceAttribute,
                    awacs.ec2.ModifySnapshotAttribute,
                    awacs.ec2.RegisterImage,
                    awacs.ec2.RunInstances,
                    awacs.ec2.StopInstances,
                    awacs.ec2.TerminateInstances
                ])
            if 'rds' in requested_permissions:
                actions_to_dedupe.extend(CFN_ACTIONS)
                actions_to_dedupe.extend([
                    awacs.aws.Action('rds', 'Describe*'),
                    awacs.ec2.AuthorizeSecurityGroupEgress,
                    awacs.ec2.AuthorizeSecurityGroupIngress,
                    awacs.ec2.CreateSecurityGroup,
                    awacs.ec2.DeleteSecurityGroup,
                    awacs.ec2.DescribeAccountAttributes,
                    awacs.ec2.DescribeSecurityGroups,
                    awacs.ec2.DescribeVpcs,
                    awacs.ec2.RevokeSecurityGroupEgress,
                    awacs.ec2.RevokeSecurityGroupIngress,
                    awacs.ec2.UpdateSecurityGroupRuleDescriptionsEgress,
                    awacs.ec2.UpdateSecurityGroupRuleDescriptionsIngress,
                    awacs.rds.AddTagsToResource,
                    awacs.rds.CreateDBInstance,
                    awacs.rds.CreateDBInstanceReadReplica,
                    awacs.rds.CreateDBSnapshot,
                    awacs.rds.DeleteDBInstance,
                    awacs.rds.ModifyDBInstance,
                    awacs.rds.RebootDBInstance,
                    awacs.rds.RestoreDBInstanceFromDBSnapshot
                ])
            if 'core' in requested_permissions:
                actions_to_dedupe.extend(CFN_ACTIONS)
                actions_to_dedupe.extend([
                    awacs.ec2.CreateKeyPair,
                    awacs.ec2.DescribeKeyPairs,
                    awacs.ec2.ImportKeyPair,
                    awacs.s3.CreateBucket,
                    awacs.s3.DeleteBucket,
                    awacs.s3.DeleteBucketPolicy,
                    awacs.s3.PutBucketPolicy,
                    awacs.s3.PutBucketVersioning,
                    awacs.s3.PutLifecycleConfiguration,
                    awacs.ec2.AllocateAddress,
                    awacs.ec2.DescribeAddresses,
                    awacs.s3.ListBucket,
                    awacs.aws.Action('s3', 'ListObjects'),
                    awacs.s3.PutObject,
                    awacs.iam.GetRole,
                    awacs.ssm.DescribeDocument,
                    awacs.ssm.CreateDocument,
                    awacs.ssm.DeleteDocument,
                    awacs.sns.GetTopicAttributes,
                    awacs.sns.CreateTopic,
                    awacs.sns.DeleteTopic,
                    awacs.ec2.DescribeVpcs,
                    awacs.ec2.CreateVpc,
                    awacs.ec2.ModifyVpcAttribute,
                    awacs.ec2.DeleteVpc,
                    awacs.ec2.DescribeInternetGateways,
                    awacs.ec2.CreateInternetGateway,
                    awacs.ec2.AttachInternetGateway,
                    awacs.ec2.DetachInternetGateway,
                    awacs.ec2.DeleteInternetGateway,
                    awacs.ec2.DescribeNatGateways,
                    awacs.ec2.CreateNatGateway,
                    awacs.ec2.DeleteNatGateway,
                    awacs.ec2.ReleaseAddress,
                    awacs.ec2.CreateTags,
                    awacs.ec2.DescribeAvailabilityZones,
                    awacs.ec2.DescribeAccountAttributes,
                    awacs.ec2.DescribeRouteTables,
                    awacs.ec2.CreateRouteTable,
                    awacs.ec2.AssociateRouteTable,
                    awacs.ec2.DeleteRouteTable,
                    awacs.ec2.CreateRoute,
                    awacs.ec2.DeleteRoute,
                    awacs.ec2.DescribeSubnets,
                    awacs.ec2.CreateSubnet,
                    awacs.ec2.DeleteSubnet,
                    awacs.awslambda.CreateFunction,
                    awacs.awslambda.GetFunctionConfiguration,
                    awacs.awslambda.DeleteFunction,
                    awacs.cloudwatch.PutMetricAlarm,
                    awacs.ec2.DescribeSecurityGroups,
                    awacs.ec2.CreateSecurityGroup,
                    awacs.ec2.AuthorizeSecurityGroupEgress,
                    awacs.ec2.AuthorizeSecurityGroupIngress,
                    awacs.ec2.RevokeSecurityGroupEgress,
                    awacs.ec2.DeleteSecurityGroup
                ])
            if 'asg' in requested_permissions:
                actions_to_dedupe.extend(CFN_ACTIONS)
                actions_to_dedupe.extend([
                    awacs.autoscaling.CreateAutoScalingGroup,
                    awacs.autoscaling.CreateLaunchConfiguration,
                    awacs.autoscaling.DeleteAutoScalingGroup,
                    awacs.autoscaling.DeleteLaunchConfiguration,
                    awacs.autoscaling.DescribeAutoScalingGroups,
                    awacs.autoscaling.DescribeAutoScalingInstances,
                    awacs.autoscaling.DescribeLaunchConfigurations,
                    awacs.autoscaling.DescribeScalingActivities,
                    awacs.autoscaling.UpdateAutoScalingGroup,
                    awacs.awslambda.InvokeFunction,
                    awacs.iam.AddRoleToInstanceProfile,
                    awacs.iam.AttachRolePolicy,
                    awacs.iam.CreateInstanceProfile,
                    awacs.iam.CreatePolicy,
                    awacs.iam.CreateRole,
                    awacs.iam.DeleteInstanceProfile,
                    awacs.iam.DeletePolicy,
                    awacs.iam.DeleteRole,
                    awacs.iam.DeleteRolePolicy,
                    awacs.iam.DetachRolePolicy,
                    awacs.iam.GetPolicy,
                    awacs.iam.ListPolicyVersions,
                    awacs.iam.PassRole,
                    awacs.iam.PutRolePolicy,
                    awacs.iam.RemoveRoleFromInstanceProfile
                ])
            if 'serverless' in requested_permissions:
                actions_to_dedupe.extend(CFN_ACTIONS)
                actions_to_dedupe.extend([
                    awacs.awslambda.CreateAlias,
                    awacs.awslambda.CreateFunction,
                    awacs.awslambda.DeleteAlias,
                    awacs.awslambda.DeleteFunction,
                    awacs.awslambda.GetFunction,
                    awacs.awslambda.GetFunctionConfiguration,
                    awacs.awslambda.ListVersionsByFunction,
                    awacs.awslambda.PublishVersion,
                    awacs.awslambda.UpdateAlias,
                    awacs.awslambda.UpdateFunctionCode,
                    awacs.awslambda.UpdateFunctionConfiguration,
                    awacs.iam.CreateRole,
                    awacs.iam.DeleteRole,
                    awacs.iam.DeleteRolePolicy,
                    awacs.iam.GetRole,
                    awacs.iam.PassRole,
                    awacs.iam.PutRolePolicy,
                    awacs.logs.CreateLogGroup,
                    awacs.logs.DeleteLogGroup,
                    awacs.logs.DescribeLogGroups
                ])
                iam_statements.extend([
                    Statement(
                        Action=[
                            awacs.s3.CreateBucket,
                            awacs.s3.DeleteBucket,
                            awacs.s3.GetAccelerateConfiguration,
                            awacs.s3.ListBucket,
                            awacs.s3.ListBucketVersions,
                            awacs.s3.PutAccelerateConfiguration
                        ],
                        Effect=Allow,
                        Resource=[
                            "arn:aws:s3:::*-serverlessdeploymentbucket-*"
                        ]
                    ),
                    Statement(
                        Action=[
                            awacs.s3.DeleteObject,
                            awacs.s3.GetObject,
                            awacs.s3.PutObject
                        ],
                        Effect=Allow,
                        Resource=[
                            "arn:aws:s3:::*-serverlessdeploymentbucket-*/*"
                        ]
                    )
                ])
            if actions_to_dedupe:
                iam_statements.append(
                    Statement(
                        # Order preserving dedupe adapted from:
                        # https://stackoverflow.com/a/17016257
                        Action=list(OrderedDict.fromkeys(actions_to_dedupe)),
                        Effect=Allow,
                        Resource=['*']
                    )
                )

        policy = template.add_resource(
            iam.ManagedPolicy(
                'Policy',
                Description='Managed policy for platform modules.',
                Path='/',
                PolicyDocument=PolicyDocument(Version='2012-10-17',
                                              Statement=iam_statements)
            )
        )
        template.add_output(
            Output(
                "%sArn" % policy.title,
                Description='Managed policy Arn',
                Value=Ref(policy)
            )
        )


def version():
    """Call version function from top of repo."""
    root_dir = dirname(dirname(dirname(dirname(realpath(__file__)))))
    if root_dir not in sys.path:
        sys.path.append(root_dir)
    import platform_version  # pylint: disable=import-error
    return platform_version.version()


# Helper section to enable easy blueprint -> template generation
# (just run `python <thisfile>` to output the json)
if __name__ == "__main__":
    from stacker.context import Context

    print(PlatformPolicy('test',
                         Context({'namespace': 'test'}),
                         None).to_json())
