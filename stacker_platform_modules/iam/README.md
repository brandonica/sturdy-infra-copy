# IAM

This module creates shared/common IAM resources.

## platform_policy

Consume in your environment with a config like the following to create a managed policy (output name `PolicyArn`) capable of managing the resources specified in IamPermissions (more details below):
```
namespace: ${namespace}
stacker_bucket: ${stacker_bucket_name}

package_sources:
  git:
    - uri: git@bitbucket.org:nbdev/sturdy-platform-infrastructure.git
      tag: vX.X.X
      paths:
        - stacker_platform_modules/iam

stacks:
  onicaplatform-mgmt-policy:
    class_path: iam_blueprints.platform_policy.PlatformPolicy
    variables:
      IamPermissions: cloudformation,packer
      # EnvironmentName: ${environment}
      # ConfigBucketArn: ${xref ${customer}-common-core-chefbuckets::ChefBucketArn}
      # BucketKey: someclassofserver
```

### IamPermissions

`IamPermissions` is a comma-separated list of permissions to be added to the policy.

* `stacker_bucket`: Allows management of any S3 bucket starting with `stacker-`
* `chef_configs`: Allows cookbook archives to be uploaded to Chef config bucket
* `ssm`: Allows for management of SSM parameters under ENV.BUCKETKEY.* (e.g. `common.vpnservers.*`)
* `cloudformation`: Basic CFN management
* `packer`: Adds [basic permissions for running Packer.](https://www.packer.io/docs/builders/amazon.html#using-an-iam-task-or-instance-role)
* `serverless`: Allows [Serverless](https://serverless.com/) app deployment (includes `cloudformation` automatically)
* `core`: Adds permissions for deploying the `core` Sturdy Platform module (includes `stacker_bucket` & `cloudformation` automatically)
* `asg`: Adds permissions for deploying the `asg` Sturdy Platform module
* `rds`: Adds permissions for deploying the `rds` Sturdy Platform module
