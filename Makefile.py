#!/usr/bin/env python
"""Project Makefile, but in Python."""

from contextlib import contextmanager
from optparse import OptionParser
from subprocess import check_call, check_output

import json
import os
import stat
import sys
import tarfile

import platform_version   # pylint: disable=relative-import

REPO_ROOT = os.path.dirname(os.path.abspath(__file__))
ARCHIVE_FILE = ('sturdy-platform-infrastructure-'
                "%s.tar.gz" % platform_version.version())
# Config file generated with `pylint --generate-rcfile > .pylintrc`
PYLINT_RC_FILE = os.path.join(REPO_ROOT, '.pylintrc')


@contextmanager
def change_dir(newdir):
    """Change directory.

    Adapted from http://stackoverflow.com/a/24176022

    """
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)


def tar():
    """Generate repo tarball."""
    def tarfilter(tarinfo):
        """Filter and modify tarinfo during tar creation."""
        if tarinfo.name in ['.git', 'bitbucket-pipelines.yml']:
            return None
        tarinfo.uid = tarinfo.gid = 0
        tarinfo.uname = tarinfo.gname = 'root'
        return tarinfo

    with change_dir(REPO_ROOT):
        check_call(['find', './', '-iname', '*.pyc', '-delete'])
        check_call(['find',
                    './',
                    '-maxdepth',
                    '1',
                    '-iname',
                    'sturdy-platform-infrastructure-*.tar.gz',
                    '-delete'])
        with tarfile.open(ARCHIVE_FILE,
                          'w:gz') as archive:
            archive.add('./',
                        arcname=os.path.sep,
                        filter=tarfilter)


def upload():
    """Generate and upload repo tarball."""
    import boto3

    tar()
    archive = open(os.path.join(REPO_ROOT, ARCHIVE_FILE), 'rb')
    s3_client = boto3.client('s3')
    s3_client.put_object(Bucket='sturdy-platform-releases',
                         Key="infrastructure/%s" % ARCHIVE_FILE,
                         Body=archive)
    archive.close()


def lint():
    """Call code linters."""
    python_dirs = ['src', 'stacker_platform_modules']
    # Sanity check for directory existence
    for i in python_dirs:
        if not os.path.isdir(os.path.join(REPO_ROOT, i)):
            print "%s does not appear to be valid directory" % i
            sys.exit(1)
    # Invoke linters
    with change_dir(REPO_ROOT):
        check_call(['flake8'] + python_dirs)
        check_call(['pydocstyle'] + python_dirs)
        check_call(['yamllint', '--config-file=.yamllint.yml'] + python_dirs)


def unittest():
    """Run unit tests."""
    for root, dirs, files in os.walk(REPO_ROOT):  # pylint: disable=W0612
        for name in files:
            if name[-3:] == ".py":
                # pylint
                filepath = os.path.join(root, name)
                check_call(['pylint',
                            "--rcfile=%s" % PYLINT_RC_FILE,
                            '-E',  # only notify on errors
                            filepath])
                # Blueprints should output their CFN json when directly run
                if (root.endswith('blueprints') and
                        not filepath.endswith('__init__.py')):
                    if not stat.S_IXUSR & os.stat(filepath)[stat.ST_MODE]:
                        print 'Error: File %s is not executable' % filepath
                        sys.exit(1)
                    try:
                        json.loads(check_output([filepath]))
                    except:  # noqa: E722
                        print ("Error while checking %s for valid JSON "
                               "output" % filepath)
                        raise


def test():
    """Lint and perform unit tests."""
    lint()
    unittest()


if __name__ == "__main__":
    PARSER = OptionParser()
    (OPTIONS, ARGS) = PARSER.parse_args()

    # Loop through positional arguments and call the associated functions
    for arg in ARGS:
        locals()[arg]()
