"""Version module for package."""
__version__ = '1.21.4'


def version():
    """Return current version number."""
    return __version__
