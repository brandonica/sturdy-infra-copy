"""Lambda HTTP Checker."""

from datetime import datetime
import logging
import threading
from botocore.vendored import requests
import boto3

LOGGER = logging.getLogger(__name__)
# Set to DEBUG to see details in lambda logs
# Set to ERROR for production
LOGGER.setLevel('ERROR')

CONST_TRUE = 0
CONST_FALSE = 1
CW_NAMESPACE = 'Sturdy Lambda Checks'


def push_metric(metric_name, dimension, value):
    """Publish values to Cloudwatch Metrics."""
    LOGGER.info('Pushing metric %s ("Check ID" dimension %s) with val %s',
                metric_name,
                dimension,
                value)
    cw_client = boto3.client('cloudwatch')
    cw_client.put_metric_data(
        Namespace=CW_NAMESPACE,
        MetricData=[
            {
                'MetricName': metric_name,
                'Dimensions': [
                    {
                        'Name': 'Check ID',
                        'Value': dimension
                    }
                ],
                'Timestamp': datetime.now(),
                'Value': value
            },
        ]
    )


def check_http(data):  # pylint: disable=too-many-branches
    """Check http connections."""
    LOGGER.debug('Running check_http with %s', data)

    # Check if we want to verify the SSL certificate
    if 'verify_ssl' in data:
        verify_ssl = data['verify_ssl']
    else:
        verify_ssl = True

    # Do a request to the URL
    if 'auth' in data:
        req = requests.get(data['url'],
                           verify=verify_ssl,
                           auth=(data['auth']['user'],
                                 data['auth']['pass']))
    else:
        req = requests.get(data['url'], verify=verify_ssl)

    # Push metrics for response time
    push_metric('HTTPResponseTime',
                data['id'],
                req.elapsed.total_seconds())
    LOGGER.info('HTTPResponseTime %s', req.elapsed.total_seconds())

    # Check for status_code and report status to cloudwatch
    if 'status_code' in data:
        if req.status_code == int(data['status_code']):
            push_metric('HTTPResponseCodeMatch', data['id'], CONST_TRUE)
        else:
            LOGGER.error('%s check failed expected %s - %s',
                         'HTTPResponseCodeMatch',
                         data['status_code'],
                         req.status_code)
            push_metric('HTTPResponseCodeMatch', data['id'], CONST_FALSE)

    push_metric('HTTPResponseCode', data['id'], req.status_code)

    # Check for headers and report status to cloudwatch
    if 'headers' in data:
        for header, val in data['headers'].items():
            if val in req.headers[header]:
                push_metric('HTTPHeadersMatch', data['id'], CONST_TRUE)
            else:
                LOGGER.error('%s check failed expected - %s',
                             'HTTPHeadersMatch',
                             val)
                push_metric('HTTPHeadersMatch', data['id'], CONST_FALSE)

    # Check for content and report status to cloudwatch
    if 'content' in data:
        for name, val in data['content'].items():
            if val in req.content:
                push_metric('HTTPContentMatch',
                            "%s/%s" % (data['id'], name),
                            CONST_TRUE)
            else:
                LOGGER.error('%s check failed expected - %s',
                             'HTTPContentMatch', val)
                push_metric('HTTPContentMatch', data['id'], CONST_FALSE)


def lambda_handler(event, context):  # pylint: disable=unused-argument
    """Perform simple http checks."""
    threads = []

    if 'checks' in event and event['checks']:
        for check in event['checks']:
            LOGGER.info('Starting thread...%s', check['id'])
            thread = threading.Thread(target=check_http, args=[check])
            threads.append(thread)
            thread.start()

            for i in threads:
                i.join()
